from text_alignment_tool import ChunkAlignmentAlgorithm, SimpleChunkAlignmentAlgorithm, ChunkAlignmentDifflib,ChunkAlignmentLevenshtein,ChunkAlignmentJaroWinkler, TextAlignmentTool, EpidocScraper, AltoFileLoader
import os








#STEP 1) LOADING THE QUERY AND TARGET
#======================================================================================================================================================
#Loading the bad alto file you're going to correct.(the HTR)
#the pk parameter (pk of the papyrus's containing document) is only necessary if you're planning to reupload the corrected alto back to escriptorium automatically.
#if all you need is correct it and then upload manually, call the function without calling the pk of the document containing
#the image/document part you're aligning.

#"alto_htr.xml" here and the pk "1234" are just examples. Use your own downloaded alto file and
#pass as argument the pk of the escriptorium document from which it was downloaded.

#If you want to align many alto htr files or a whole directory just adjust the script accordingly.

loader_alto_query=AltoFileLoader("alto_htr.xml", pk=1234)


#scraping , the transcription of a papyrus passing its TM number as an argument and aligning it with the corresponding alto containing the rough HTR transcription
#you've downloaded from escriptorium and you've stored locally in your working directory.

loader_alto_target=EpidocScraper(60671)









#STEP 2) INSTANCIATING THE ALIGNMENT TOOL AND ALIGNMENT ALGORITHM
#=======================================================================================================================================================
#Instanciating a TextAlignmentTool instance:

align=TextAlignmentTool(loader_alto_query, loader_alto_target)

#Instanciating an alignment algorithm. Choose any algorithm you want. Here we're using ChunkAlignmentLevenshtein()

algo_1=ChunkAlignmentLevenshtein()



#STEP 3) ALIGNING
#=======================================================================================================================================================
#Aligning

align.align_text(algo_1)

print(algo_1)



#You now have to choose a way to align(choose one of the two functions below (either choice one or choice 2, but not both):



####################Choice 1) you want to use the update_lines() function to correct the alto file.


query_chunks=align.latest_alignment.input_query_text_chunk_indices
target_chunks=align.latest_alignment.input_target_text_chunk_indices
chunk_map=algo_1.get_chunk_map
ground_truth=align.latest_alignment_target

#Calling the function to match/align the bad lines of the HTR to the good ones(target text/transcription):
loader_alto_query.update_lines(chunk_map=chunk_map, query_text_chunks= query_chunks, target_text_chunks= target_chunks, ground_truth_text=ground_truth)



######################Choice 2) Alternatively you might prefer using the update_content() function which aligns better on a character level
#(but might unfortunately move some characters at the extremities to following or previous lines)
#for an optimal line to line matching use update_lines() instead, see Choice 1
#Warning:Do not use both functions consecutively as it could mess up the results, just choose one of the two for each alignment (either update_lines() as in Choice 1 above or update_content())


#query_chunks=align.latest_alignment.input_query_text_chunk_indices
#target_chunks=align.latest_alignment.input_target_text_chunk_indices
#chunk_map=algo_1.get_chunk_map
#ground_truth=align.latest_alignment_target
#unaligned=align.latest_alignment_unaligned_chunks
#alignment_pairs=align.latest_alignment.input_output_alignment

#loader_alto_query.update_content(
#	alignment_pairs=alignment_pairs, 
#	query_text_chunks= query_chunks, 
#	target_text_chunks= target_chunks, 
#	ground_truth_text=ground_truth, 
#	unaligned_ground_truth_chunks=unaligned)




#=======================================================================================================================================================
#Visualize the dictionary of the updated alto lines

print(loader_alto_query.show_alto_lines_updated)
print(loader_alto_query.show_alto_lines)



#STEP 4)CORRECTING THE ALTO FILE 
#=======================================================================================================================================================
#Correcting the alto file itself (meaning correcting your local alto file that is meant to be corrected and reuploaded, in this example "alto_htr.xml")
loader_alto_query.update_file()




#STEP 5)UPLOADING BACK TO ESCRIPTORIUM (OPIONAL, CAN ALTERNATIVELY BE DONE MANUALLY)
#=======================================================================================================================================================
#Upload the file back to eScriptorium
#For this to work you need to have instanciated the query loader using the pk of the document that contains the image/papyrus/manuscript you're aligning
#see line 10 of current scriptm in this case the bad alto originates from escriptorium document
#with pk number 1234: `loader_alto_query=AltoFileLoader("alto_htr.xml", pk=1234)` 

#In the function that uploads the corrected file back to eScriptorium, you need two parameters, your username and your password at eScriptorium to be able to import the
#corrected alto file back to its source document.

#loader_alto_query.upload(user="yourusername",password="yourpassword")
