#Scraping transcriptions corresponding to xml alto files of Greek papyri's rough transcription downloaded from eScriptorium.

from text_alignment_tool import EpidocScraper
import os
import glob
os.mkdir('doc_100')

#directory containing the downloaded xml/alto files for which we need official transcriptions. Those xmls could contain the rough transcription by the HTR model of the papyri in question.
path="./documentary"

#iterating through directory containing the downloaded xml/alto files for which we need official transcriptions. Those xmls could contain the rough transcription by the HTR model of the papyri in question,
#so we might need the processed transcriptions in text format to add them manually into eScriptorium:
for file in os.listdir(path):
	#finding a pattern to retrieve the TM number out of the file name. Without the TM we can't do much, it's the unique identifier of the papyrus:
	tm_number=file.split("__")[0]
	#Instanciating an EpidocScraper instance, affecting it to variable `scraper`:
	scraper=EpidocScraper(tm_number)
	#By calling the output property makes of the EpidocScraper instance, the text loader actually loads the text by calling the load() function (namely `self._load()`).
	scraper.output
	#changing directory to go into the newly created directory and save the scraped text there:
	os.chdir('doc_100')
	#calling the save_text() function and saving as a txt file , the name of which will be the TM number. If multiple files have the same TM (recto and verso etc), you'll see them named  `{TM}_1`, `{TM}_2`
	#etc, but the contained transcription will be the same since we generate a unique text content/transcription for each TM.
	#If the transcription has not been found you'll get an empty text file.
	scraper.save_text()
	#switching back to our initial working directory:
	os.chdir('..')


os.chdir('doc_100')

#Creating a merged transcription file containing all the transcriptions scraped above:

files=glob.glob('*.txt')

with open('merged.txt', 'w', encoding='utf-8') as merged:
	for file_ in files:
		for line in open(file_,'r', encoding="utf-8"):
			merged.write(line)
