import numpy as np
from typing import Tuple


def pairwise_equal_array(str_1: np.ndarray, str_2: np.ndarray) -> Tuple[bool, str]:
    equal = np.array_equal(str_1, str_2)
    if equal:
        return equal, ""

    longest, shortest = (str_1, str_2) if str_1.size > str_2.size else (str_2, str_1)
    exception = ""
    for idx, pair in enumerate(zip(longest, shortest)):
        if pair[0] != pair[1]:
            string_1 = "".join([chr(x) for x in str_1])
            string_2 = "".join([chr(x) for x in str_2])
            exception = f"""
"{string_1}"
does not match
"{string_2}"
"{chr(pair[0])}" != "{chr(pair[1])}" at index {idx}."""
            break
    return equal, exception
