# TODO: Maybe test the AltoXMLTextLoader?

from text_alignment_tool import (
    TextLoader,
    StringTextLoader,
    NewlineSeparatedTextLoader,
    AltoXMLTextLoader,
    LetterList,
    TextChunk,
)
from .helper_utils import pairwise_equal_array
from deepdiff import DeepDiff
from typing import List
import numpy as np
import pytest


string_loader = (
    np.array([ord(x) for x in "input text"]),
    [TextChunk(list(range(10)), "full text")],
    StringTextLoader("input text"),
)

newline_text_loader = (
    np.array([ord(x) for x in "input text second row"]),
    [TextChunk(list(range(11)), "line 0"), TextChunk(list(range(11, 21)), "line 1")],
    NewlineSeparatedTextLoader("input text \nsecond row"),
)


@pytest.mark.parametrize(
    "expected_output,expected_text_chunks,loader", [string_loader, newline_text_loader]
)
def test_text_loader(
    expected_output: LetterList,
    expected_text_chunks: List[TextChunk],
    loader: TextLoader,
):
    matching_output, msg = pairwise_equal_array(loader.output, expected_output)
    assert matching_output, msg
    assert not DeepDiff(expected_text_chunks, loader.text_chunk_indices)
