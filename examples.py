# Import the tool and necessary classes
from text_alignment_tool import (
    TextAlignmentTool,
    StringTextLoader,
    NewlineSeparatedTextLoader,
    RemoveEnclosedTransformer,
    GlobalAlignmentAlgorithm,
    LocalAlignmentAlgorithm,
    ChunkAlignmentAlgorithm,
    NeedleInHaystackAlgorithm,
)

query_text_2 = """the[article] very[adverb] quick[adjective]
brown[adjective] fox[noun]
jumped[verb] over[preposition]
the[article] lazy[adjective] dog[noun]"""
query_2 = NewlineSeparatedTextLoader(query_text_2)
target_text_2 = """the quick
golden brown fox
jumped over
the lazy dog"""
target_2 = NewlineSeparatedTextLoader(target_text_2)

aligner_2 = TextAlignmentTool(query_2, target_2)
remove_bracket_words = RemoveEnclosedTransformer("[", "]")
aligner_2.query_text_transform(remove_bracket_words)
# or more than one transformer as a list
# aligner_2.query_text_transforms([remove_bracket_words])
local_alignment_algorithm = ChunkAlignmentAlgorithm()
aligner_2.align_text(local_alignment_algorithm)
alignments_2 = aligner_2.collect_all_alignments()
print("\n\n".join([y.visualize_chunk_alignment for x in alignments_2 for y in x]))
print(remove_bracket_words)
