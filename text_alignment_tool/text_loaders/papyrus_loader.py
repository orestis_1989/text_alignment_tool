#The purpose here is to load the papyrus transcription from a file stored locally and then perform a chunk alignment with the rough OCR transcription.
#Each missing caracter is replaced by an underscore`_`. Otherwise the user can change the "missing attribute in the class, for example choose to 
#replace by a hyphen : `-`.
#Lines missing from the original can be included or not. The default attribute is for them to not be included.
#The loader only imports diacritics that are present in the original.
#If you want all the diacritics of the transcription, you can use the FullEpidocLoader instead which loads everything.


from typing import List
from text_alignment_tool.shared_classes import TextChunk, LetterList
from text_alignment_tool.text_loaders.text_loader import TextLoader
import numpy as np
import requests
import os
from urllib.request import urlopen
import lxml.etree as et
from unicodedata import normalize
from unicodedata import category
from PIL import Image
from collections import Counter
from io import BytesIO
import json
import math




class PapyrusLoader(TextLoader):


	def __init__(self,file="", directory="", missing="_", norm="NFD", include=False,  interlinear=False, symbols=True, strip=True, capitalize=True, level=4, doc=None, part=None, token=None):

		super().__init__()
		self._directory=directory
		self._file_name=file
		self._missing_character=missing
		self._max_level=level
		#Modify the following parameter if you need NFD, NFKC or NFKD.If you do not set a normalization standard then NFD is set by default.
		self._normalization_standard=norm
		self._include_gap_lines=include
		self._interlinear_markup=interlinear	
		self.__input_text=""
		#Transcription file found or not:
		self._found=False
		self._part_pk=part
		self._doc_pk=doc
		self._symbols=symbols
		self.__user_token=token
		self._no_spaces=strip
		self._capitalize=capitalize
		
		

	def _load(self) -> LetterList:
		epidoc_final_text=""
		text_chunks=[]
		start_chunk_idx=0

		#Mapping the punctuation included in the original papyrus in order to avoid losing it 
		#when cleaning the text from punctuation that is added by the editor. (see further into the code of the loader).
		punctuation_mapping={"∎":"'","∷":"⁚","∘":"·","⫽":"//","⫶":".","⩩":"⸎","∴":"’","∵":"’","∫":"⸐","⨍":"˙"}

		#The numbering of the lines will be internal to the loader we choose not to keep the @n attribute of the transcription
		#We're not interested in the original numbering of the lines in the TEI
		chunk_count=0	

		#The transcription of the papyrus in question will be accessible in a local directory.If you're running the script from within the directory and 
		#in which the .xml transcriptions and you're looking to extract papyri one by one then you should
		# just instanciate the loader with the specific file name (example: loader_1=PapyrusLoader(file="10495.xml") or loader_1=PapyrusLoader(file="POxy.001.xml"). 
		#Otherwise, if you're outside of the folder containing the transcriptions you should also specify the directory.
		#In this case you should use an absolute path 
		#unless you're running the script from within the parent directory of the directory containing the transcriptions (example if you're running the script from "Documents" and the folder containing the xmls
		#is "transcriptions" and is a folder in "Documents", then instanciate like that loader_1=PapyrusLoader("transcriptions"). Otherwise you should instanciate, using an absolute path like this:
		#loader_1=PapyrusLoader(dir="C:\\Users\\exampleuser\\Documents\\transcriptions", file="10495.xml"). This is just an example to illustrate an absolute path, your own path will surely differ.
		

		
		
		try:
			#Parsing the XML of the transcription
			path=self._file_name
			with open(path, encoding="utf-8") as f:
				tree=et.parse(f)
				root=tree.getroot()
				#Transcription file found and transcription about to get parsed:
				self._found=True
			
		except FileNotFoundError:
			try:
				path=os.path.join(self._directory,self._file_name)
				#Parsing the XML of the transcription
				with open(path, encoding="utf-8") as f:
					tree=et.parse(f)
					root=tree.getroot()
					#Transcription file found and transcription about to get parsed:
					self._found=True
			except FileNotFoundError:
				print("no transcription found")
				raise

			
						

		
		#Cleaning the elements' text from unnecessary spaces before and after the text content:
		for element_to_clean in root.findall('.//{http://www.tei-c.org/ns/1.0}*'):
			if element_to_clean.text:
				element_to_clean.text=element_to_clean.text.strip() 
			if element_to_clean.tail:
				element_to_clean.tail=element_to_clean.tail.strip() 

		#Adding the tick character for it to be loaded in the text later
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}*[@rend="tick"]'):
			if element_to_correct.text:
				element_to_correct.text=element_to_correct.text.strip() 

				#element_to_correct.text+="'"
				#Instead of an actual tick we use another symbol and we're replacing it later. This avoids the tick getting removed when 
				#we're going to go through the process of removing the punctuation that is not on the papyrus
				#but is in the edition.We're gonna use symbols of the unicode category "Sm" which are math symbols.
				#We do the same for some other punctuation signs belonging to the "Po" and the "Pf" unicode categories
				#See further into the code.
				element_to_correct.text+="∎"

		#Adding the diple obelismene symbol as text inside the milestone element for it to be loaded in the text later
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}milestone[@rend="diple-obelismene"]'):
			element_to_correct.text='>'

		#Adding the diple obelismene symbol as text inside the milestone element for it to be loaded in the text later
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}milestone[@rend="diple-obelismene"]'):
			element_to_correct.text='⤚'

		#Adding the paragraphos. We also add a linebreak since the paragraphos is considered as a separate line in 
		#the way Holger Essler processes the transcriptions . We do not add a linebreak in the end since there seems to be a linebreak 
		#after a paragraphos milestone in most cases (this particular part of the code can be improved however). 
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}milestone[@rend="paragraphos"]'):
			#element_to_correct.text='\r⸐'
			element_to_correct.text="\r∫"
		#Adding the cross
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="stauros"]'):
			element_to_correct.text="†"

		#Adding the rho-cross
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="rho-cross"]'):
			element_to_correct.text="⳨"

		#Adding chirho. This correction has to be repeated for whichever other symbols are present in the papyri as soon as we encounter any.
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="chirho"]'):
			element_to_correct.text="☧"

		#Majuscule Greek Χ for the check symbol, in order to avoid inserting characters from other languages. 
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="check"]'):
			element_to_correct.text="Χ"

		#Adding the middot.
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="middot"]'):
			#element_to_correct.text="·". We're instead replacing this with a middot later on to avoid conflicting
			#in the phase of edition punctuation removal, where we remove punctuation not present
			#on the papyrus.
			element_to_correct.text="∘"

		#For filler strokes present in the original, adding 'ߟ'
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="filler"]'):
			element_to_correct.text="ߟ"

		#Apostrophe. 
		#For the apostrophe, you can find examples in transcriptions on papyri.info here:
		#https://papyri.info/dclp/59268/source and here  https://papyri.info/dclp/59268/source
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="apostrophe"]'):
			#element_to_correct.text="’"
			element_to_correct.text="∴"
			
		#Diastole
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="diastole"]'):
			#element_to_correct.text="’"
			element_to_correct.text="∵"
			
		#Dipunct
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="dipunct"]'):
			#element_to_correct.text="for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="dipunct"]'):
			#element_to_correct.text="⁚"	
			element_to_correct.text="∷"
					
		#High punctus
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="high-punctus"]'):
			#element_to_correct.text="˙"
			element_to_correct.text="⨍"

		#Low punctus	
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="low-punctus"]'):
			#element_to_correct.text="."
			element_to_correct.text="⫶"

		#Opening parenthesis present in the original
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="parens-punctuation-opening"]'):
			element_to_correct.text="("
			
		#Closing parenthesis present in the original
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="parens-punctuation-closing"]'):
			element_to_correct.text=")"
			
		#Editorial coronis
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="coronis"]'):
			#element_to_correct.text="⸎"
			element_to_correct.text="⩩"

		#Dash
		#This one doesn't need mapping to a math symbol since it's not among the unicode categories that get removed (see further into the code) and doesn't
		#get deleted at the phase of punctuation removal. So there's no risk to lose it.
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="dash"]'):
			element_to_correct.text="‒"

		#Anti-stigma
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="anti-stigma"]'):
			element_to_correct.text="Ͻ"

		#Use this papyrus to test in any case https://papyri.info/ddbdp/sb;26;16648/source.
		for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}hi[@rend="supraline"]'):
			if element_to_correct.text:
				new_text_with_combining_overline=""
				for char in element_to_correct.text:
					new_text_with_combining_overline +="̅"+char
				element_to_correct.text=new_text_with_combining_overline
				
		#Removing the <supplied>, the <reg>, the <rdg> (except when it corresponds to what's written on the papyrus), the <lem> when it's purely a result of the edition, 
		#the <corr> , the <figure>, the <desc> and the <ex> elements. This way we only keep the visible and the unclear text but not lost text or corrections of the editor. 
		#We do keep the <del> as well, except for the <del rend="corrected">.
		for element_to_del in root.findall('.//{http://www.tei-c.org/ns/1.0}supplied'):
			for element in element_to_del.getchildren():
				element.getparent().remove(element)
			element_to_del.text=""		

		

		#When the <lem> corresponds to the editor's variant, the way the papyrologists
		#seem to encode it ,it gets an @resp attribute (the content of which is the name and info of the editor) that doesn't start with a `P` or a `p`.
		#When the <lem> corresponds to the editor's variant and not what's seen on the papyrus, we remove it.
		#If on the other hand there's no @resp attribute at all, we keep the <lem>.As a matter of fact,
		#if the @resp is absent, then the <lem> corresponds to what's seen on the papyrus ,almost always.
		
		for element_to_del in root.findall('.//{http://www.tei-c.org/ns/1.0}lem[@resp]'):
			#Additional if condition to avoid "index out of range" error.We also verify that there's at least an <rdg> left inside the <app> 
			if element_to_del.get("resp"):
				if element_to_del.get("resp")[0] not in ["P","p"]:
					#Here we do not just delete the children elements of the <lem> as we do in most other unwanted elements we process( eg: <supplied>, <corr> etc).
					#Here, we rather completely delete the <lem> element itself. We do that to process the <rdg> elements better (see further into the code).
					element_to_del.getparent().remove(element_to_del)
					
		
		#Now that we've processed the <lem> elements we process the <rdg> elements.
		#When the <rdg> corresponds exactly to the content of the papyrus we keep it, otherwise we remove it, unless the lem itself has been removed.
		for element_to_del in root.findall('.//{http://www.tei-c.org/ns/1.0}rdg'):
			#getting the <app> element (parent element of the <lem>) in order to use it in one of the conditions:
			app_element=element_to_del.getparent()
			#processing the <lem> itself:
			if element_to_del.get("resp"):
				#"P" and "p" for papyrus
				if element_to_del.get("resp")[0] not in ["P","p"]:
					for element in element_to_del.getchildren():
						element.getparent().remove(element)
					element_to_del.text=""
			#If no resp attribute at all, deletion, we only keep the <lem> . However, if there's no <lem> we do not delete the <rdg>, hence the condition.
			elif app_element.findall(".//{http://www.tei-c.org/ns/1.0}lem"):
				for element in element_to_del.getchildren():
					element.getparent().remove(element)
				element_to_del.text=""

		#<reg>
		for element_to_del in root.findall('.//{http://www.tei-c.org/ns/1.0}reg'):
			for element in element_to_del.getchildren():
				element.getparent().remove(element)
			element_to_del.text=""
		
		#<corr>
		for element_to_del in root.findall('.//{http://www.tei-c.org/ns/1.0}corr'):
			for element in element_to_del.getchildren():
				element.getparent().remove(element)
			element_to_del.text=""

		#<ex>
		for element_to_del in root.findall('.//{http://www.tei-c.org/ns/1.0}ex'):
			ex_has_sibling_elements=False
			for expan_subelement in element_to_del.getparent().getchildren():
				if expan_subelement.tag!="{http://www.tei-c.org/ns/1.0}ex":
					ex_has_sibling_elements=True
			if not self._symbols or element_to_del.getparent().tag!="{http://www.tei-c.org/ns/1.0}expan" or element_to_del.getparent().text is not None or element_to_del.tail is not None or ex_has_sibling_elements==True:
				for element in element_to_del.getchildren():
					element.getparent().remove(element)				
				element_to_del.text=""

			#otherwise we add a parenthesis around it to make it easier for the DNN to detect an expan corresponding to a symbol/abbreviation.
			else:
				element_to_del.text=f"({element_to_del.text})"
		
		#<desc>
		for element_to_del in root.findall('.//{http://www.tei-c.org/ns/1.0}desc'):
			for element in element_to_del.getchildren():
				element.getparent().remove(element)
			element_to_del.text=""

		for element_to_del in root.findall('.//{http://www.tei-c.org/ns/1.0}del[@rend="corrected"]'):
			for element in element_to_del.getchildren():
				element.getparent().remove(element)
			element_to_del.text=""

		for element_to_del in root.findall('.//{http://www.tei-c.org/ns/1.0}figure'):
			for element in element_to_del.getchildren():
				element.getparent().remove(element)
			element_to_del.text=""
			
		#Preparing the query to extract the transcription text from the <text> section of the xml. If we default to query_alternative_2(see below) the only
		#problem is we risk getting extra content(for example content from text/body/head) or getting some content twice(in the case of nested divs).
		#That said, the division into divs in the papyri.info transcriptions is not 100% consistent thus making it impossible to include all possible cases
		#as alternative queries in the present loader.
		#The @type attribute of the divs containing the text of the transcription are not consistent among papyri. Sometimes we have an @corresp, other 
		#papyri have an @type="edition" etc. 
		#Later we'll attempt to match our image on escriptorium to one of the original images.
		

		#TEI namespace			
		namespace='{http://www.tei-c.org/ns/1.0}'
		if not root.findall('.//{http://www.tei-c.org/ns/1.0}div[@corresp]') or not root.findall('.//{http://www.tei-c.org/ns/1.0}graphic') or self._doc_pk==None or self._part_pk==None :
		
		#xpath query to look for transcription text later
			query=".//"+namespace+"div[@corresp]"
			query_alternative_1=".//"+namespace+"div[@type='edition']"
			query_alternative_2=".//"+namespace+"text//"+namespace+"div"
			queries=[query] if len(root.findall(query)) else ([query_alternative_1] if len(root.findall(query_alternative_1)) else  [query_alternative_2])

		else:
			graphics=root.findall('.//{http://www.tei-c.org/ns/1.0}graphic')
			graphic_url_list=[]
			for graphic in  graphics:
				url=graphic.get("url")
				graphic_url_list.append(url)
				
			#Getting the eScriptorium image using the API
			doc=int(self._doc_pk)
			part=int(self._part_pk)
			url_escr=f"https://msia.escriptorium.fr/api/documents/{doc}/parts/{part}"
			if "Token" not in self.__user_token:
				token=f"Token {self.__user_token}"
			else:
				token=f"{self.__user_token}"

			#headers
			headers={"Content-type":"application/json", "Accept":"application/json","Authorization": token}
			request=requests.get(url_escr,headers=headers)
			obtained_data=request.json()
			image_info=obtained_data["image"]["uri"]
			image_url=f"https://msia.escriptorium.fr/{image_info}"
			image_escr=requests.get(image_url)
			image_escr=Image.open(BytesIO(image_escr.content))
			image_escr=np.asarray(image_escr)
			image_escr=image_escr.flatten()

			#generating histogram:
			counter_1=Counter(image_escr)
			
			
			histo_1=[]
			for i in range(256):
				if i in counter_1.keys():
					histo_1.append(counter_1[i])
				else:
					histo_1.apend(0)

			#Dictionary of distances between the eScriptorium image and the images
			distances={}

			#Calculating distances for each graphic url:
			for graphic_url in graphic_url_list:

				#The berlpap urls have changed:
				
				if "ww2" in graphic_url:
					
					url_part_2=graphic_url.split("ww2")[1]
					graphic_url=f"https://berlpap{url_part_2}"
				graphic=requests.get(graphic_url)
				graphic=Image.open(BytesIO(graphic.content))
				graphic=np.asarray(graphic)
				graphic=graphic.flatten()

				#Generating histogram to compare to eScriptorium image's histogram:
				counter_2=Counter(graphic)

				#Normalizing counter_2
				histo_2=[]
				for i in range(256):
					if i in counter_2.keys():
						histo_2.append(counter_2[i])
					else:
						histo_2.append(0)
				distance=0

				#Euclidean distance:
				for i in range(len(histo_1)):
					distance+=math.pow((histo_1[i]-histo_2[i]),2)
				distance=math.sqrt(distance)

				#what if two distances are the same?we keep adding 0.01( needs improvement, not very good practice).However it has to be said that having two images that are equidistant to the eScriptorium image is a one in a million possibility. 
				while distance in distances.keys():
					distance+=0.01
				distances[distance]=graphic_url

			#Choosing among the graphics(images) in the <graphic> tag in the epidoc the one that best matches the image on eScriptorium:						
			minimal_distance=min(list(distances.keys()))
			matching_image_url=distances[minimal_distance]
			
			#Now we can find our corresponding div(s):
			text_parts=[]			
			for graphic in graphics:
				
				url=graphic.get("url")
				
				corresp=graphic.getparent().get("corresp")
				#Matching the urls to the correct corresp, while also handling the berlpap issue where the urls seems to have changed.
				#There's also the http:// vs https:// which can cause a false non match:
				
				if (url.split("//")[1]==matching_image_url.split("//")[1]) or (url.split("ww2.")[1]==matching_image_url.split("berlpap.")[1]):
					
					text_parts.append(corresp)
			
			#And our query in this case is:
			
			queries=[]
			if text_parts:				
				for text_part in text_parts:
					attribute_condition=f"@corresp='{text_part}'"
					query=f".//{namespace}div[{attribute_condition}]"
					queries.append(query)

			#Needs improvement, need to add more alternative_queries.
			else:
				query=".//"+namespace+"div[@type='edition']"	
				queries.append(query)
			
		#We now add linebreaks properly following the <lb> element. Even if there's a break="no" attribute.
		#As a matter of fact, linebreaks are detected as text nodes of content("\n") by the parser each time a new line is started in the page. However we add our own linebreaks
		#this way we're sure the <lb> in the TEI will become a linebreak. We use \r to differentiate from \n (the latter being already present)
		for line in root.findall('.//{http://www.tei-c.org/ns/1.0}lb'):
			line.text="\r"

		#The horizontal rule is going to get counted as part of the above line. Take that into account while segmenting:
		for hrz_rule in root.findall('.//{http://www.tei-c.org/ns/1.0}milestone[@rend="horizontal-rule"]'):
			hrz_rule.text="――――――――"

		#In case we need the loader to add gap(lost) lines to the input text, we set the missing_line attribute of the loader to True when instanciating the loader loader.
		#In that case:
		if self._include_gap_lines:
			for missing_line in root.findall(".//{http://www.tei-c.org/ns/1.0}gap[@unit='line']"):
				n_lines=int(missing_line.get("quantity")) if missing_character.get("quantity") else 1
				for i in range(n_lines):

					#papyri.info by convention doesn't seem to add a new line, they just include the missing line in the line before or the line after:
					#missing_line.text="\r"+"-- -- -- -- -- -- -- -- -- --"
					missing_line.text="-- -- -- -- -- -- -- -- -- --"

		#What we need to process next are the missing characters:
		for missing_character in root.findall(".//{http://www.tei-c.org/ns/1.0}gap[@unit='character']"):
			n_characters=int(missing_character.get("quantity")) if missing_character.get("quantity") else 1
			for i in range(n_characters):
				missing_character.text=self._missing_character				
				
		#Now we're finally going to look for the text of the transcription.		
		for query in queries:

			
			
			
			if len(root.findall(query)):
				for div in root.findall(query):
					joined_text=""
					text_matches=div.xpath('.//text()')
				
					#List of diacritics included in the epidoc
					diacritics=diacritics={"acute":"́", "grave":"̀", "circumflex":"͂", "asper":"̔", "lenis":"̓", "diaeresis":"̈"}
					include_diacritics=False
					for text_match in text_matches:

						#the <add @place> element can have several levels of nested content. We capture most cases by
						#considering for each text node, its parent element(usual position of the add tag), its grandparent, 
						#great grandparent elements and so on.
						parent_element=text_match.getparent()

						#Handling diacriticals. Keeping the diacritics of the original (to be used later, see line 494).	
						text_match_parent=text_match.getparent()


						level=1
						#We're going to go upwards from node to parent node up to a certain point in case we find an @place attribute.
						#The point we stop is determined by self._max_level attribute, which by default is 3.
						while not parent_element.get("place") and level<=self._max_level:
							if parent_element.getparent() is not None:
								parent_element=parent_element.getparent()
							level+=1
						
						#The interlinears will be added as extra text chunks. Nevertheless, they'll be kept in the content of the
						#You can test with https://papyri.info/ddbdp/p.matr;;2/source (it contains an add place="above").
						#Needs improvement: condition to check if it's truly a subelement of an <add> or simply some other tag containing an @place
						#See here for text and tail using lxml :https://lxml.de/tutorial.html
						#ATTENTION: IF YOU USE A TRANSFORMER THAT REMOVES EVERYTHING EXCEPT THE CHARACTERS, YOU'RE GOING TO LOSE THE BELOW '<' AND THE  '>'
						
						if parent_element.get("place") and parent_element.tail!=text_match and self._interlinear_markup==True:	
							#One possibility is to tag the interlinear text as done here, however the matching will be affected a lot:
							text_match=f'<interlinear: {text_match_parent.get("place")}>{text_match_parent.text}</interlinear: {text_match_parent.get("place")}>{text_match_parent.tail}'

						#Instead we can simply add square brackets around the interlinear.
						
						elif parent_element.get("place") and parent_element.tail!=text_match:	
							text_match=f"[{text_match}]"	
							
												
						if text_match_parent.get("rend") in diacritics.keys() and text_match_parent.text==text_match:
							include_diacritics=True	
							
						#Removing diacriticals that are not present in the original(we first normalize to NFD for that and then we insert the corresponding
						#diacritical (indicated by the @rend attribute) to the text).
						if not include_diacritics:
							joined_text=""
							joined_text_with_diacritics=normalize("NFD",text_match)
							for c in joined_text_with_diacritics:
								if category(c)=="Mn" or category(c)=="Sk" or category(c)=="Po" or category(c)=="Pf" or category(c)=="Lm":
									continue
								if c in punctuation_mapping.keys():
									joined_text+=punctuation_mapping[c]
								else:
									joined_text+=c
								

						#otherwise we only remove the punctuation and leave the diacriticals:
						else:
							joined_text=""
							joined_text_with_diacritics=normalize("NFD",text_match)
							for c in joined_text_with_diacritics:
								if category(c)=="Mn" or category(c)=="Sk" or category(c)=="Po" or category(c)=="Pf" or category(c)=="Lm":
									continue
								if c in punctuation_mapping.keys():
									joined_text+=punctuation_mapping[c]
								else:
									joined_text += c+diacritics[text_match_parent.get("rend")]
							
							
						#Re-initialising the boolean to False for the next iteration of the for loop:
						include_diacritics=False
						epidoc_final_text+=joined_text
		epidoc_final_text=epidoc_final_text.replace("\n","")
		if self._capitalize:
			epidoc_final_text=epidoc_final_text.upper()
			
		#Multiple spaces, if present, transformed to single space. I avoid using regex in order not to mess with the linebreaks.
		#Otherwise it is much better practice to use regex.
		for idx, char in enumerate(epidoc_final_text):
			idx_following=idx+1
			if char==" ":
				while len(epidoc_final_text)>idx_following and epidoc_final_text[idx_following]==' ':				
					epidoc_final_text=epidoc_final_text[0 : idx_following] + epidoc_final_text[idx+2 : :]
						
		#Re-normalizing to the desired normalisation standard(attribute of the loader itself)
		epidoc_final_text=normalize(self._normalization_standard,epidoc_final_text)	
		
		self.__input_text=epidoc_final_text

		#Splitting the text into chunks using our own line division:
		for chunk_match in self.__input_text.split("\r"):
			
			chunk_length=len(chunk_match)

			#If line is not empty (in the sense of a real empty line intended to be empty, and not a missing line in the sense of a <gap>):
			if chunk_length!=0:
				text_chunks.append(TextChunk(list(range(start_chunk_idx,start_chunk_idx+chunk_length)), f" line {chunk_count}"))
						
				chunk_count+=1
				start_chunk_idx+=chunk_length
			else:
				chunk_count+=1
				start_chunk_idx += chunk_length
			#if text_chunks:
				#print(text_chunks[-1].indices)

		#It could be that the papyrus is simply not transcribed. In that case:
		if not text_chunks:
			self.__input_text=""
		self._output=np.array([ord(x) for x in self.__input_text if x!="\r"])
		self._input_output_map=[(x,x) for x in range(0,self.output.size)]
		self._text_chunk_indices=text_chunks

		return super()._load()



	#Check if transcription file to parse has been found
	@property
	def found(self):
		return self._found



