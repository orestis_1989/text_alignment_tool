from typing import List
from text_alignment_tool.shared_classes import TextChunk, LetterList
from text_alignment_tool.text_loaders.text_loader import TextLoader
import numpy as np


class SimpleTxtLoader(TextLoader):
	def __init__(self, file_path):
		super().__init__()
		self.__input_text =""
		self._file_path=file_path

	def _load(self) -> LetterList:
		self._output = np.array([ord(x) for x in self.__input_text])
		text_chunks: List[TextChunk] = []
		start_chunk_idx = 0
		line_count = 0
		with open(self._file_path, 'r', encoding='utf-8') as f:
			lines=f.readlines()
		for line in lines:
			self.__input_text+=line        
			line_length = len(line)
			if line_length!=0:
				text_chunks.append(TextChunk(list(range(start_chunk_idx, start_chunk_idx + line_length)),f"line {line_count}",))
				
				line_count += 1
				start_chunk_idx += line_length
		self._input_output_map = [(x, x) for x in range(0, self.output.size)]
		self._text_chunk_indices = text_chunks
		

		return super()._load()
