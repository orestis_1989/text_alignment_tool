#Scrapes papyri of the DCLP and DDBDP databases of papyri.info using their Trismegistos (TM) number, which is a unique identifier.
#The purpose here is to scrape the whole transcription and then perform a chunk alignment with the rough OCR transcription.
#Each missing caracter is replaced by an underscore`_`. Otherwise the user can change the "missing attribute in the class, for example choose to 
#replace by a hyphen : `-`.
#Lines missing from the original can be included or not. The default attribute is for them to not be included.
#The loader only imports diacritics that are present in the original.
#If you want all the diacritics of the transcription, you can use the FullEpidocLoader instead which loads everything.
#Interlinear content is extracted and shown like this in the loaded text : `[content]`
#If a line is empty you'll see an empty line in the transcription.

import os
from typing import List
from text_alignment_tool.shared_classes import TextChunk, LetterList
from text_alignment_tool.text_loaders.text_loader import TextLoader
import numpy as np
import requests
from urllib.request import urlopen
from urllib.error import HTTPError as url_error
import lxml.etree as et
from unicodedata import normalize
from unicodedata import category
from PIL import Image
from collections import Counter
from io import BytesIO
import json
import math






class EpidocScraper(TextLoader):


    def __init__(self,tm_number, missing="", norm="NFD", include=False, interlinear=False, symbols=True, capitalize = True, strip = True, level = 4, doc=None, part=None, token=None):

        super().__init__()
        self._tm_number=tm_number
        self._missing_character=missing
        #Modify the following parameter if you need NFD, NFKC or NFKD.If you do not set a normalization standard then NFD is set by default.
        self._normalization_standard=norm
        self._include_gap_lines=include
        self._interlinear_markup=interlinear	
        self.__input_text=""
        self._found=False
        self._part_pk=part
        self._doc_pk=doc
        self.__user_token=token
        self._symbols=symbols
        self._capitalize=capitalize
        self._no_spaces=strip
        #Maximum level of search for a parent element that has the @place attribute. Default is 4, which means we're going to look
        #for that attribute in element/node's parent node, parent's parent node and so on, up to 4 levels upwards. See further into the code
        #how the interlinear additions (<add @place>) are handled in the present loader.
        self._max_level=level

    def _load(self) -> LetterList:
        epidoc_final_text=""
        text_chunks=[]
        start_chunk_idx=0

        #Mapping the punctuation included in the original papyrus in order to avoid losing it 
        #when cleaning the text from punctuation that is added by the editor. (see further into the code of the loader).
        punctuation_mapping={"∎":"'","∷":"⁚","∘":"·","⫽":"//","⫶":".","⩩":"⸎","∴":"’","∵":"’","∫":"⸐","⨍":"˙"}

        #The numbering of the lines will be internal to the loader we choose not to keep the @n attribute of the transcription
        #We're not interested in the original numbering of the lines in the TEI
        chunk_count=0	

        #The transcription of the papyrus in question will be accessible in either one of the two datatabases (ddbdp or dclp). 
        #The papyroLogos project deals mainly with those two. If we're dealing with one of the DCLP papyri, the transcription
        #can be directly accessed using the TM. If on the other hand we're dealing with a DDBDP papyrus, we first need to access its metadata
        #which are on the HGV database, and then use that metadata(in this case the id of the papyrus) in order to access the url 
        #containing the XML of the transcription.

        #DCLP url:
        address_a='https://papyri.info/dclp/'

        #HGV url(this database contains metadata concerning DDBDP papyri):
        address_b='https://papyri.info/hgv/'

        #DDBDP url:
        address_c='https://papyri.info/ddbdp/'

        #We'll need those two below to add them to the basic url above and make up the full url we're looking for, depending on the papyrus in question:
        address_part_2=str(self._tm_number)
        address_part_3='/source'

        #Checking if we're in the simple scenario (papyrus part of DCLP project)
        url_1=address_a+address_part_2+address_part_3
        if requests.get(url_1).status_code==200 or requests.get(url_1).status_code==201:
            url=url_1

        #If on the other hand it's a DDBDP papyrus, we first need to access the HGV database using its TM number, extract its ID from the Epidoc, and then use that ID 
        #to access the Epidoc of the transcription on DDBDP
        else:
            metadata_url=address_b+address_part_2+address_part_3
            metadata_alternative_url_a=address_b+address_part_2+'a'+address_part_3
            metadata_alternative_url_b=address_b+address_part_2+'b'+address_part_3
            if requests.get(metadata_url).status_code==200 or requests.get(metadata_url).status_code==201:
                try:
                    with urlopen(metadata_url) as f:
                        metadata_tree=et.parse(f)
                        root_metadata=metadata_tree.getroot()
                        papyrus_id=root_metadata.find(".//{http://www.tei-c.org/ns/1.0}idno[@type='ddb-hybrid']").text
                        #Let's clean the text of the <idno type='ddb-hybrid'> tag in case any undesired extra spaces are present.
                        papyrus_id=papyrus_id.strip()
                        url=address_c+papyrus_id+address_part_3
                except url_error:
                    #Generating for the scraper a pseudo-output text consisting of three spaces in order to escape errors in case of empty output
                    #especially when iterating over lots of files:
                    self._output=np.array([ord(x) for x in "   "])
                    print("Transcription url not reachable: no transcription ID found in HGV/DDBDP database. \n Alignment process will be impossible. \n Please try aligning another papyrus \n or modify the current loader to look into other databases.")
                    #Function returns in case transcription not found due to HTTP error:
                    return 1

            #Needs improvement to handle more cases. Basically in very rare cases a papyrus is labelled as {TMnumber}a or {TMnumber}b. 
            #Testing {TMnumber}a
            elif requests.get(metadata_alternative_url_a).status_code==200 or requests.get(metadata_alternative_url_a).status_code==201:
                try:
                    metadata_url=metadata_alternative_url_a
                    with urlopen(metadata_url) as f:
                        metadata_tree=et.parse(f)
                        root_metadata=metadata_tree.getroot()
                        papyrus_id=root_metadata.find(".//{http://www.tei-c.org/ns/1.0}idno[@type='ddb-hybrid']").text
                        #Let's clean the text of the <idno type='ddb-hybrid'> tag in case any undesired extra spaces are present.
                        papyrus_id=papyrus_id.strip()
                        url=address_c+papyrus_id+address_part_3
                except url_error:
                    #Generating for the scraper a pseudo-output text consisting of three spaces in order to escape errors in case of empty output
                    #especially when iterating over lots of files:
                    self._output=np.array([ord(x) for x in "   "])
                    print("Transcription url not reachable: no transcription ID found in HGV metadata database. \n Alignment process will be impossible. \n Please try aligning another papyrus \n or modify the current loader to look into other databases.")
                    #Function returns in case transcription not found due to HTTP error:
                    return 1
            
            #If {TMnumber}a not found then defaulting to {TMnumber}b
            else:
                try:
                    metadata_url=metadata_alternative_url_b
                    with urlopen(metadata_url) as f:
                        metadata_tree=et.parse(f)
                        root_metadata=metadata_tree.getroot()
                        papyrus_id=root_metadata.find(".//{http://www.tei-c.org/ns/1.0}idno[@type='ddb-hybrid']").text
                        #Let's clean the text of the <idno type='ddb-hybrid'> tag in case any undesired extra spaces are present.
                        papyrus_id=papyrus_id.strip()
                        url=address_c+papyrus_id+address_part_3
                except url_error:
                    #Generating for the scraper a pseudo-output text consisting of three spaces in order to escape errors in case of empty output
                    #especially when iterating over lots of files:
                    self._output=np.array([ord(x) for x in "   "])
                    print("Transcription url not reachable:  no transcription ID found in HGV metadata database. \n Alignment process will be impossible. \n Please try aligning another papyrus \n or modify the current loader to look into other databases.")
                    #Function returns in case transcription not found due to HTTP error:
                    return 1
                
        
        #Parsing the XML of the transcription
        try:
            with urlopen(url, timeout=100) as f:
                tree=et.parse(f)
                root=tree.getroot()
                #Transcription found online
                self._found=True
        #in case of urllib.error.HTTPError:
        except url_error:	
            #Generating for the scraper a pseudo-output text consisting of three spaces in order to escape errors in case of empty output
            #especially when iterating over lots of files:
            self._output=np.array([ord(x) for x in "   "])
            print("Transcription url not reachable: no transcription available in DCLP AND DDBDP databases. \n Alignment process will be impossible. \n Please try aligning another papyrus \n or modify the current loader to look into other databases.")
            #Function returns in case transcription not found due to HTTP error:
            return 1
        #Cleaning the elements' text from unnecessary spaces before and after the text content:
        for element_to_clean in root.findall('.//{http://www.tei-c.org/ns/1.0}*'):
            if element_to_clean.text:
                element_to_clean.text=element_to_clean.text.strip() 
            if element_to_clean.tail:
                element_to_clean.tail=element_to_clean.tail.strip() 

        #Adding the tick character for it to be loaded in the text later
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}*[@rend="tick"]'):
            if element_to_correct.text:
                element_to_correct.text=element_to_correct.text.strip() 
                #Instead of an actual tick we use another symbol and we're replacing it later. This avoids the tick getting removed when 
                #we're going to go through the process of removing the punctuation that is not on the papyrus
                #but is in the edition.We're gonna use symbols of the unicode category "Sm" which are math symbols.
                #We do the same for some other punctuation signs belonging to the "Po" and the "Pf" unicode categories
                #See further into the code.
                #element_to_correct.text+="'"
                element_to_correct.text+="∎"

        #Adding the diple symbol as text inside the milestone element for it to be loaded in the text later
        #This one doesn't need mapping to a math symbol since it's not among the unicode categories that get removed (see further into the code) and doesn't
        #get deleted at the phase of punctuation removal. So there's no risk to lose it.
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}milestone[@rend="diple"]'):
            element_to_correct.text='>'

        #Adding the diple obelismene symbol as text inside the milestone element for it to be loaded in the text later
        #This one doesn't need mapping to a math symbol since it's not among the unicode categories that get removed (see further into the code) and doesn't
        #get deleted at the phase of punctuation removal. So there's no risk to lose it.
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}milestone[@rend="diple-obelismene"]'):
            element_to_correct.text='⤚'

        #Adding the paragraphos. We also add a linebreak since the paragraphos is considered a separate line in 
        #the way Holger Essler processes the transcriptions . We do not add a linebreak in the end since there seems to be a linebreak 
        #after each paragraphos milestone in most cases (this particular part of the code can be improved however).
        #The transcriptions are either using a paragraphos ("⸏") or a forked paragraphos ("⸐") but those two are not 
        #differentiated on papyri.info. We choose the forked paragraphos to differentiate from other similar signs/punctuation.
        #!Consider removing the linebreak, sometimes useless and paragraphos not followed by a lineabreak on the papyrus !
        #As for the other symbols, category being 'Po', we map the symbol to '∫' (which is a math symbol and doesn't risk getting removed
        #when we're going to remove the punctuation ('Po' unicode category).We'll replace it afterwards at the extraction phase(see further).
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}milestone[@rend="paragraphos"]'):
            #element_to_correct.text='\r⸐'
            element_to_correct.text="\r∫"
        #Adding the cross.
        #This one doesn't need mapping to a math symbol since it's not among the unicode categories that get removed (see further into the code) and doesn't
        #get deleted at the phase of punctuation removal. So there's no risk to lose it.
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="stauros"]'):
            element_to_correct.text="†"

        #Adding the middot(mapping it to another symbol to avoid stripping it off when cleaning the text).We'll then
        #add it again to the transcription.
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="middot"]'):
            #element_to_correct.text="·"
            element_to_correct.text="∘"

        #Adding chirho. This correction has to be repeated for whichever other symbols are present in the papyri as soon as we encounter any.
        #This one doesn't need mapping to a math symbol since it's not among the unicode categories that get removed (see further into the code) and doesn't
        #get deleted at the phase of punctuation removal. So there's no risk to lose it.
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="chirho"]'):
            element_to_correct.text="☧"

        #Adding the rho-cross
        #This one doesn't need mapping to a math symbol since it's not among the unicode categories that get removed (see further into the code) and doesn't
        #get deleted at the phase of punctuation removal. So there's no risk to lose it.
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="rho-cross"]'):
            element_to_correct.text="⳨"

        #Majuscule Greek Χ for the check symbol, in order to avoid inserting characters from other languages. 
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="check"]'):
            element_to_correct.text="Χ"

        #Adding the slanted stroke		
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="slanting-stroke"]'):
            #element_to_correct.text="//"
            element_to_correct.text="⫽"

        #Adding the anti-stigma
        #This one doesn't need mapping to a math symbol since it's not among the unicode categories that get removed (see further into the code) and doesn't
        #get deleted at the phase of punctuation removal. So there's no risk to lose it.
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="anti-stigma"]'):
            element_to_correct.text="Ͻ"

        #For filler strokes present in the original, adding U+2015 horizontal bar
        #changed 21/6/2022 to 'ߟ' to harmonize with symbol choices of Holger Essler.
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="filler"]'):
            element_to_correct.text="ߟ"

        #Apostrophe. 
        #For the apostrophe, you can find examples in transcriptions on papyri.info here:
        #https://papyri.info/dclp/59268/source and here  https://papyri.info/dclp/59268/source
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="apostrophe"]'):
            #element_to_correct.text="’"
            element_to_correct.text="∴"
            
        #Diastole
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="diastole"]'):
            #element_to_correct.text="’"
            element_to_correct.text="∵"
            
        #Dipunct
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="dipunct"]'):
            #element_to_correct.text="⁚"	
            element_to_correct.text="∷"		

        #High punctus
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="high-punctus"]'):
            #element_to_correct.text="˙"
            element_to_correct.text="⨍"

        #Low punctus	
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="low-punctus"]'):
            #element_to_correct.text="."
            element_to_correct.text="⫶"

        #Opening parenthesis present in the original
        #This one doesn't need mapping to a math symbol since it's not among the unicode categories that get removed (see further into the code) and doesn't
        #get deleted at the phase of punctuation removal. So there's no risk to lose it.
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="parens-punctuation-opening"]'):
            element_to_correct.text="("
            
        #Closing parenthesis present in the original
        #This one doesn't need mapping to a math symbol since it's not among the unicode categories that get removed (see further into the code) and doesn't
        #get deleted at the phase of punctuation removal. So there's no risk to lose it.
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="parens-punctuation-closing"]'):
            element_to_correct.text=")"
            
        #Editorial coronis
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="coronis"]'):
            #element_to_correct.text="⸎"
            element_to_correct.text="⩩"

        #Dash
        #This one doesn't need mapping to a math symbol since it's not among the unicode categories that get removed (see further into the code) and doesn't
        #get deleted at the phase of punctuation removal. So there's no risk to lose it.
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}g[@type="dash"]'):
            element_to_correct.text="‒"


        
        #Use this papyrus to test in any case https://papyri.info/ddbdp/sb;26;16648/source.
        for element_to_correct in root.findall('.//{http://www.tei-c.org/ns/1.0}hi[@rend="supraline"]'):
            if element_to_correct.text:
                new_text_with_combining_overline=""
                for char in element_to_correct.text:
                    new_text_with_combining_overline +="̅"+char
                element_to_correct.text=new_text_with_combining_overline

        
        #Removing the <supplied>, the <reg>, the <rdg> (except when it corresponds to what's written on the papyrus), the <lem> when it's purely a result of the edition, 
        #the <corr> , the <figure>, the <desc> and the <ex> elements. This way we only keep the visible and the unclear text but not lost text or corrections of the editor. 
        #We do keep the <del> as well, except for the <del rend="corrected">.
        for element_to_del in root.findall('.//{http://www.tei-c.org/ns/1.0}supplied'):
            for element in element_to_del.getchildren():
                element.getparent().remove(element)
            element_to_del.text=""

        #When the <lem> corresponds to the editor's variant, the way the papyrologists
        #seem to encode it ,it gets an @resp attribute (the content of which is the name and info of the editor) that doesn't start with a `P` or a `p`.
        #When the <lem> corresponds to the editor's variant and not what's seen on the papyrus, we remove it.
        #If on the other hand there's no @resp attribute at all, we keep the <lem>.As a matter of fact,
        #if the @resp is absent, then the <lem> corresponds to what's seen on the papyrus ,almost always.
        
        for element_to_del in root.findall('.//{http://www.tei-c.org/ns/1.0}lem[@resp]'):
            #Additional if condition to avoid "index out of range" error.We also verify that there's at least an <rdg> left inside the <app> 
            if element_to_del.get("resp"):
                if element_to_del.get("resp")[0] not in ["P","p"]:
                    #Here we do not just delete the children elements of the <lem> as we do in most other unwanted elements we process( eg: <supplied>, <corr> etc).
                    #Here, we rather completely delete the <lem> element itself. We do that to process the <rdg> elements better (see further into the code).
                    element_to_del.getparent().remove(element_to_del)
                    
        
        #Now that we've processed the <lem> elements we process the <rdg> elements.
        #When the <rdg> corresponds exactly to the content of the papyrus we keep it, otherwise we remove it, unless the lem itself has been removed.
        for element_to_del in root.findall('.//{http://www.tei-c.org/ns/1.0}rdg'):
            #getting the <app> element (parent element of the <lem>) in order to use it in one of the conditions:
            app_element=element_to_del.getparent()
            #processing the <lem> itself:
            if element_to_del.get("resp"):
                #"P" and "p" for papyrus
                if element_to_del.get("resp")[0] not in ["P","p"]:
                    for element in element_to_del.getchildren():
                        element.getparent().remove(element)
                    element_to_del.text=""
            #If no resp attribute at all, deletion, we only keep the <lem> . However, if there's no <lem> we do not delete the <rdg>, hence the condition.
            elif app_element.findall(".//{http://www.tei-c.org/ns/1.0}lem"):
                for element in element_to_del.getchildren():
                    element.getparent().remove(element)
                element_to_del.text=""

        #<reg>
        for element_to_del in root.findall('.//{http://www.tei-c.org/ns/1.0}reg'):
            for element in element_to_del.getchildren():
                element.getparent().remove(element)
            element_to_del.text=""

        #<corr>
        for element_to_del in root.findall('.//{http://www.tei-c.org/ns/1.0}corr'):
            for element in element_to_del.getchildren():
                element.getparent().remove(element)
            element_to_del.text=""
        
        #Removing any content in the <ex> tags unless it's a symbol and we want symbols to be transcribed in full letters. If we do want the transliteration of the symbols to be include in the transcription
        #in full letters then instanciate the loaders with attribute "symbols" set to True.
        #As an improvement , mappings of all symbols should be made and the loader should replace those terms in parentheses (representing the respective symbols)
        #See with Holger Essler for more details concerning the chosen symbols.
        for element_to_del in root.findall('.//{http://www.tei-c.org/ns/1.0}ex'):
            ex_has_sibling_elements=False
            for expan_subelement in element_to_del.getparent().getchildren():
                if expan_subelement.tag!="{http://www.tei-c.org/ns/1.0}ex":
                    ex_has_sibling_elements=True
            if not self._symbols or element_to_del.getparent().tag!="{http://www.tei-c.org/ns/1.0}expan" or element_to_del.getparent().text is not None or element_to_del.tail is not None or ex_has_sibling_elements==True:
                for element in element_to_del.getchildren():
                    element.getparent().remove(element)				
                element_to_del.text=""
            #Processing the <ex> element's content. The <ex> is only going to be extracted and used for the alignment if the expan in question 
            #is a symbol but not if it is an abbreviation!(see above). In any case, we're putting the text inside parentheses to be able to differentiate 
            #it from the rest of the text, and for the training to be able to take it into account.
            #We add a parenthesis around it to make it to also make it easier for the DNN to detect an expan corresponding to a symbol/abbreviation.
            else:
                element_to_del.text=f"({element_to_del.text})"
        
        #<desc>
        for element_to_del in root.findall('.//{http://www.tei-c.org/ns/1.0}desc'):
            for element in element_to_del.getchildren():
                element.getparent().remove(element)
            element_to_del.text=""

        for element_to_del in root.findall('.//{http://www.tei-c.org/ns/1.0}del[@rend="corrected"]'):
            for element in element_to_del.getchildren():
                element.getparent().remove(element)
            element_to_del.text=""

        for element_to_del in root.findall('.//{http://www.tei-c.org/ns/1.0}figure'):
            for element in element_to_del.getchildren():
                element.getparent().remove(element)
            element_to_del.text=""
            
        #Preparing the query to extract the transcription text from the <text> section of the xml. If we default to query_alternative_2(see below) the only
        #problem is we risk getting extra content(for example content from text/body/head) or getting some content twice(in the case of nested divs).
        #That said, the division into divs in the papyri.info transcriptions is not 100% consistent thus making it impossible to include all possible cases
        #as alternative queries in the present loader.
        #The @type attribute of the divs containing the text of the transcription are not consistent among papyri. Sometimes we have an @corresp, other 
        #papyri have an @type="edition" etc. 
        #Further we'll attempt to match our image on escriptorium to one of the original images.
        

        #TEI namespace			
        namespace='{http://www.tei-c.org/ns/1.0}'
        if not root.findall('.//{http://www.tei-c.org/ns/1.0}div[@corresp]') or not root.findall('.//{http://www.tei-c.org/ns/1.0}graphic') or self._doc_pk==None or self._part_pk==None :
        #xpath query to look for transcription text later
            query=".//"+namespace+"div[@corresp]"
            query_alternative_1=".//"+namespace+"div[@type='edition']"
            query_alternative_2=".//"+namespace+"text//"+namespace+"div"
            queries=[query] if len(root.findall(query)) else ([query_alternative_1] if len(root.findall(query_alternative_1)) else  [query_alternative_2])

        else:
            graphics=root.findall('.//{http://www.tei-c.org/ns/1.0}graphic')
            graphic_url_list=[]
            for graphic in  graphics:
                url=graphic.get("url")
                graphic_url_list.append(url)
                
            #Getting the eScriptorium image using the API
            doc=int(self._doc_pk)
            part=int(self._part_pk)
            url_escr=f"https://msia.escriptorium.fr/api/documents/{doc}/parts/{part}"
            if "Token" not in self.__user_token:
                token=f"Token {self.__user_token}"
            else:
                token=f"{self.__user_token}"

            #headers
            headers={"Content-type":"application/json", "Accept":"application/json","Authorization": token}
            request=requests.get(url_escr,headers=headers)
            obtained_data=request.json()
            image_info=obtained_data["image"]["uri"]
            image_url=f"https://msia.escriptorium.fr/{image_info}"
            image_escr=requests.get(image_url)
            image_escr=Image.open(BytesIO(image_escr.content))
            image_escr=np.asarray(image_escr)
            image_escr=image_escr.flatten()

            #generating histogram:
            counter_1=Counter(image_escr)
            
            
            histo_1=[]
            for i in range(256):
                if i in counter_1.keys():
                    histo_1.append(counter_1[i])
                else:
                    histo_1.apend(0)

            #Dictionary of distances between the eScriptorium image and the images
            distances={}

            #Calculating distances for each graphic url:
            for graphic_url in graphic_url_list:
                #The berlpap urls have changed:
                
                if "ww2" in graphic_url:
                    url_part_2=graphic_url.split("ww2")[1]
                    graphic_url=f"https://berlpap{url_part_2}"
                graphic=requests.get(graphic_url)
                graphic=Image.open(BytesIO(graphic.content))
                graphic=np.asarray(graphic)
                graphic=graphic.flatten()

                #Generating histogram to compare to eScriptorium image's histogram:
                counter_2=Counter(graphic)

                #Normalizing counter_2
                histo_2=[]
                for i in range(256):
                    if i in counter_2.keys():
                        histo_2.append(counter_2[i])
                    else:
                        histo_2.append(0)
                distance=0
                
                #Euclidean distance:
                for i in range(len(histo_1)):
                    distance+=math.pow((histo_1[i]-histo_2[i]),2)
                distance=math.sqrt(distance)
                
                #what if two distances are the same?we keep adding 0.01( needs improvement, not very good practice).However it has to be said that having two images that are equidistant to the eScriptorium image is a one in a million possibility. 
                while distance in distances.keys():
                    distance+=0.01
                distances[distance]=graphic_url

            #Choosing among the graphics(images) in the <graphic> tag in the epidoc the one that best matches the image on eScriptorium:						
            minimal_distance=min(list(distances.keys()))
            matching_image_url=distances[minimal_distance]
            
            #Now we can find our corresponding div(s):
            text_parts=[]			
            for graphic in graphics:
                
                url=graphic.get("url")				
                corresp=graphic.getparent().get("corresp")

                #Matching the urls to the correct corresp, while also handling the berlpap issue where the urls seems to have changed.
                #There's also the http:// vs https:// which can cause a false non match:				
                if (url.split("//")[1]==matching_image_url.split("//")[1]) or (url.split("ww2.")[1]==matching_image_url.split("berlpap.")[1]):
                    text_parts.append(corresp)
            
            #And our query in this case becomes:
            
            queries=[]
            if text_parts:				
                for text_part in text_parts:
                    attribute_condition=f"@corresp='{text_part}'"
                    query=f".//{namespace}div[{attribute_condition}]"
                    queries.append(query)

            #Needs improvement, need to add more alternative_queries.
            else:
                query=".//"+namespace+"div[@type='edition']"	
                queries.append(query)
                                
            
        #We now add linebreaks properly following the <lb> element.Even when there's a break="no" attribute.
        #As a matter of fact, linebreaks are detected as text nodes of content("\n") by the parser each time a new line is started in the page. However we add our own linebreaks
        #this way we're sure the <lb> in the TEI will become a linebreak. We use \r to differentiate from \n (the latter being already present)
        for line in root.findall('.//{http://www.tei-c.org/ns/1.0}lb'):
            line.text="\r"

        #The horizontal rule is going to get counted as part of the above line just above it. Code to improve: take into account unit attribute. Take that into account while segmenting:
        for hrz_rule in root.findall('.//{http://www.tei-c.org/ns/1.0}milestone[@rend="horizontal-rule"]'):
            hrz_rule.text="――――――――"

        #In case we need the loader to add gap(lost) lines to the input text, we set the missing_line attribute of the loader to True when instanciating the loader loader.
        #In that case:
        if self._include_gap_lines:
            for missing_line in root.findall(".//{http://www.tei-c.org/ns/1.0}gap[@unit='line']"):
                n_lines=int(missing_line.get("quantity")) if missing_character.get("quantity") else 1
                for i in range(n_lines):

                    #papyri.info by convention doesn't seem to add a new line, they just include the missing line in the line before or the line after:
                    #missing_line.text="\r"+"-- -- -- -- -- -- -- -- -- --"
                    missing_line.text="-- -- -- -- -- -- -- -- -- --"

        #What we need to process next are the missing characters:
        for missing_character in root.findall(".//{http://www.tei-c.org/ns/1.0}gap[@unit='character']"):
            n_characters=int(missing_character.get("quantity")) if missing_character.get("quantity") else 1
            for i in range(n_characters):
                missing_character.text=self._missing_character				
                
        #Now we're finally going to look for the text of the transcription and extract it.		
        for query in queries:

            if len(root.findall(query)):
                for div in root.findall(query):
                    joined_text=""
                    text_matches=div.xpath('.//text()')
                
                    #List of diacritics included in the Epidoc
                    #Using the combining diacriticals (to follow Holger Essler's norms):
                    diacritics=diacritics={"acute":"́", "grave":"̀", "circumflex":"͂", "asper":"̔", "lenis":"̓", "diaeresis":"̈"}
                    include_diacritics=False
                    for text_match in text_matches:
                        #the <add @place> element can have several levels of nested content. We capture most cases by
                        #considering for each text node, its parent element(usual position of the add tag), its grandparent, 
                        #great grandparent elements and so on.
                        parent_element=text_match.getparent()
                        level=1
                        #We're going to go upwards from node to parent node up to a certain point in case we find an @place attribute.
                        #The point we stop is determined by self._max_level attribute, which by default is 3.
                        while not parent_element.get("place") and level<=self._max_level:
                            if parent_element.getparent() is not None:
                                parent_element=parent_element.getparent()
                            level+=1
                        
                        #Keeping the diacritics of the original:	
                        text_match_parent=text_match.getparent()						
                        if text_match_parent.get("rend") in diacritics.keys() and text_match_parent.text==text_match:
                            include_diacritics=True

                        #The interlinears will be added as extra text chunks. Nevertheless, they'll be kept in the content of the
                        #You can test with https://papyri.info/ddbdp/p.matr;;2/source (it contains an add place="above").
                        #Needs improvement: condition to check if it's truly a subelement of an <add> or simply some other tag containing an @place
                        #See here for text and tail using lxml :https://lxml.de/tutorial.html
                        #ATTENTION: IF YOU USE A TRANSFORMER THAT REMOVES EVERYTHING EXCEPT THE CHARACTERS, YOU'RE GOING TO LOSE THE BELOW '<' AND THE  '>'
                        
                        if parent_element.get("place") and parent_element.tail!=text_match and self._interlinear_markup==True:	
                            #One possibility is to tag the interlinear text as done here, however the matching will be affected a lot:
                            text_match=f'<interlinear: {text_match_parent.get("place")}>{text_match_parent.text}</interlinear: {text_match_parent.get("place")}>{text_match_parent.tail}'

                        #Instead we can simply add square brackets around the interlinear.						
                        elif parent_element.get("place") and parent_element.tail!=text_match:	
                            text_match=f"[{text_match}]"					
                    
                        #Removing diacriticals that are not present in the original(we first normalize to NFD for that and then we insert the corresponding
                        #diacritical (indicated by the @rend attribute) to the text).
                        
                        if not include_diacritics:
                            joined_text=""
                            joined_text_with_diacritics=normalize("NFD",text_match)
                            for c in joined_text_with_diacritics:
                                
                                #"Lm" category is a vast one, dangerous to exclude it but no other choice for now. We need to remove the diacriticals 
                                #that shouldn't be there and are just an addition of the editor.
                                if category(c)=="Mn" or category(c)=="Sk" or category(c)=="Po" or category(c)=="Pf" or category(c)=="Lm":
                                    continue
                                if c in punctuation_mapping.keys():
                                    joined_text+=punctuation_mapping[c]
                                else:
                                    joined_text+=c
                                
                        #Otherwise we only remove the punctuation but we keep the diacriticals:
                        else:
                            joined_text=""
                            joined_text_with_diacritics=normalize("NFD",text_match)
                            for c in joined_text_with_diacritics:
                                
                                if category(c)=="Mn" or category(c)=="Sk" or category(c)=="Po" or category(c)=="Pf" or category(c)=="Lm":
                                    continue
                                if c in punctuation_mapping.keys():
                                    joined_text+=punctuation_mapping[c]
                                else:
                                    joined_text += c+diacritics[text_match_parent.get("rend")]
                                    
                                                            
                        #Re-initialising the boolean to False for the next iteration of the for loop:
                        include_diacritics=False
                        epidoc_final_text+=joined_text
                        if self._capitalize:
                            epidoc_final_text=epidoc_final_text.upper()
        epidoc_final_text=epidoc_final_text.replace("\n","")
            
        
        #If we need to strip spaces (self._no_spaces=True at instanciation of the text loader)
        if self._no_spaces:
            epidoc_final_text=epidoc_final_text.replace(" ","")

        #Otherwise we just replace multiple spaces by single spaces.
        #So: multiple spaces, if present, transformed to single space. I avoid using regex in order not to mess with the linebreaks.
        #Otherwise it is much better practice to use regex.
        else:
            for idx, char in enumerate(epidoc_final_text):
                idx_following=idx+1
                if char==" ":
                    while len(epidoc_final_text)>idx_following and epidoc_final_text[idx_following]==' ':				
                        epidoc_final_text=epidoc_final_text[0 : idx_following] + epidoc_final_text[idx+2 : :]
        
        
        #Re-normalizing to the desired normalisation standard(attribute of the loader itself)
        epidoc_final_text=normalize(self._normalization_standard,epidoc_final_text)							
        self.__input_text=epidoc_final_text

        #Splitting the text into chunks using our own line division:
        for chunk_match in self.__input_text.split("\r"):
            
            chunk_length=len(chunk_match)
            #if line is not empty (in the sense of a real empty line intended to be empty, and not a missing line in the sense of a <gap>):
            if chunk_length!=0:
                text_chunks.append(TextChunk(list(range(start_chunk_idx,start_chunk_idx+chunk_length)), f" line {chunk_count}"))
                        
                chunk_count+=1
                start_chunk_idx+=chunk_length
            else:
                chunk_count+=1
                start_chunk_idx += chunk_length
            
        #It could be that the papyrus is simply not transcribed. In that case:
        #Can be improved
        if not text_chunks:
            self.__input_text=""
        self._output=np.array([ord(x) for x in self.__input_text if x!="\r"])
        #Avoiding infinite recursions (i.e. avoiding cases where self._load() would get called again and again in cases of a transcription
        #that has been found online but whose output is empty. The output property of the mother class would keep calling the _load()
        #function of the child class until it gets some output, then the child class calls the output property of the mother class,
        #which happens by default, see how self._input_output_map below calls the output property.
        #This goes like this recursively and the recursion doesn't end if there's no output).
        if not self._output.any():
            print("URL found but no text in transcription url. Please try aligning another papyrus.")
            self._output=np.array([ord(x) for x in "   "])
            #Exiting the function.
            return 1
        self._input_output_map=[(x,x) for x in range(0,self.output.size)]
        self._text_chunk_indices=text_chunks

        #returning self._output
        return super()._load()


    #Save a copy of the scraped transcription in txt format:
    def save_text(self):
        files=os.listdir('.')
        count=0
        tm=str(self._tm_number)
        tm_file_name=tm+".txt"
        #in case a transcription of a papyrus with the same TM number already exists in the directory, we add a suffix (that suffix being a number)
        while tm_file_name in files:				
            count+=1
            tm_file_name= tm + "_" + str(count) + ".txt"
        
        with open(tm_file_name, 'w', encoding="utf-8") as f:
            f.write(self.__input_text)
            
    
    #check if the transcirpion has been found online
    @property
    def found(self):
        return self._found