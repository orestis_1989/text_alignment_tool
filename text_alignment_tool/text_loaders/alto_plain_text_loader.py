#Use this alto loader to extract all the text content of the rough HTR transcription that you will have downloaded from eScriptorium.
#This would in this case be your query text loader , which would correspond to the rough transcription.
#When instanciating a TextAlignmentTool object ,the query loader should be in first position and the target loader in second position.
#add examples

#Once loaded you can try to perform, for instance , a global alignment of the whole query text (rough OCR) to the transcription. 
#You can also perform a chunk to chunk alignment using the Chunk Alignment Algorithm if both query (rough HTR) and target are loaded
#as one chunk per line , using for example the simple alto loader or an EpiDoc loader that loads the text line by line .
#Blank lines in the alto are not loaded and are left blank. it would be too risky and too complicated to align a completely blank line
#with something, the purpose of the alignment being to be as accurate as possible.


from typing import List
from text_alignment_tool.shared_classes import TextChunk, LetterList
from text_alignment_tool.text_loaders.text_loader import TextLoader
import numpy as np
import lxml.etree as et
import unicodedata


class SimpleAltoLoader(TextLoader):


	def __init__(self,file_path):

		super().__init__(file_path)
		self.__input_text=""
		

	def _load(self) -> LetterList:
		extracted_text=""
		text_chunks=[]
		start_chunk_idx=0
		tree=et.parse(self._file_path)
		root=tree.getroot()
		namespace='{http://www.loc.gov/standards/alto/ns-v4#}'
		line_element='TextLine'
		content_element='String'
		line_query='.//'+namespace+line_element
		#Find the content of the lines using xpath.We start by finding the lines and then extract their content
		text_query=namespace+content_element
		counter=0
		indices=[]
		for line in root.findall(line_query):
			line_number=line.attrib.get("ID")
			content=line.find(text_query)
			chunk_match=str(content.attrib.get("CONTENT"))
			chunk_length=len(chunk_match)
			extracted_text+=chunk_match
			if chunk_length!=0:
				text_chunks.append(TextChunk(list(range(start_chunk_idx, start_chunk_idx+chunk_length)), f"{line_number}"))
			#the incrementation can be outside the conditional if , however I kept it inside. no difference.
				start_chunk_idx += chunk_length
			
		
		self.__input_text=extracted_text
		self._output=np.array([ord(x) for x in self.__input_text])
		self._input_output_map=[(x,x) for x in range(0,self.output.size)]
		self._text_chunk_indices=text_chunks

		return super()._load()


