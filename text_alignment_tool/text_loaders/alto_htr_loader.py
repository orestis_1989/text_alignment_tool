from escriptorium_connector import EscriptoriumConnector
import os
import io
import shutil
from typing import List
from text_alignment_tool.shared_classes import TextChunk, LetterList, TextAlignments
from text_alignment_tool.text_loaders.text_loader import TextLoader
import numpy as np
import xml.etree.ElementTree as et
import unicodedata

#PURPOSE
#Use this alto loader to extract all the text content of the rough HTR transcription that you will have downloaded from eScriptorium.
#This would in this case be your query text loader , which would correspond to the rough transcription.

#!!!!!When you instanciate a TextAlignmentTool object, always put your query text (meaning the rough HTR transcription, in this case the alto loader) as first argument
#and your target text in second argument to guarantee an optimal chunk (and character) alignment.!!!!!

#Once the text is loaded from the alto file using this loader you can perform a chunk alignment of the whole query text (rough OCR) to the target(transcription). 
#You can for example perform a chunk to chunk alignment using the Chunk Alignment Algorithm or any of its variants if both query (rough HTR) and target are loaded
#as one chunk per line , using for example the present alto loader to load the query and the PapyrusLoader or the EpidocScraper to load the target. 
#Both these loaders loads the text line by line.

#PAPYROLOGOS
#In order to load the target text (the witness text, the correct text) you should use a loader that loads the text line by line
#meaning the chunks are lines and not tokens (you can use the EpidocScraper, the PapyrusLoader,
#the TxtLoader). That's the main goal of the papyri's alignment in papyroLogos.

#OTHER PROJECTS
#If on the other hand you have as a target a text whose chunks are tokens/words (which is not the case in papyroLogos) , 
#you can add to the present HTR loader a new function which replaces contents based on characters. In that case you'd probably also have to add your 
#alignment algorithm subclass as among the existing ones, the ones that could in theory work for tokens, are very slow if your target text is too long
#(like a book for example) and they probably won't work.

#EMPTY ALTO LINES
#Blank lines in the alto are loaded as an empty line or a line only including a space character (see empty line attribute). it would be too risky and too complicated to align a completely blank line
#with something, the purpose of the alignment being to be as accurate as possible.

#NORMALISATION OF THE LOADED TEXT
#Use the normalisation that you want when instanciating the loader. Default is NFD. Other possibilities are NFC,NFKD, NFKC.



















class AltoFileLoader(TextLoader):


    def __init__(self,file_path, norm="NFD", pk= None, empty_line=""):

        super().__init__(file_path)
        self.__input_text=""
        self._normalisation_standard=norm
        self._dict_alto_lines={}
        self._last_line_id=""
        self._dict_alto_lines_updated={}
        self._char_to_line_map={}
        self._document_pk=int(pk) if pk is not None else None
        self._empty_lines_content=empty_line
        

    def _load(self) -> LetterList:
        extracted_text=""
        text_chunks=[]
        start_chunk_idx=0
        tree=et.parse(self._file_path)
        root=tree.getroot()
        namespace='{http://www.loc.gov/standards/alto/ns-v4#}'
        line_element='TextLine'
        content_element='String'
        line_query='.//'+namespace+line_element

        #Find the content of the lines using xpath.We start by finding the lines and then extract their content
        text_query=namespace+content_element

        #Index of characters contained in the alto files' lines':
        char_counter=0

        for line in root.findall(line_query):
            line_id=line.attrib.get("ID")

            #updating id of last line parsed
            self._last_line_id=line_id
            content=line.find(text_query)
            chunk_match=str(content.attrib.get("CONTENT"))
            chunk_match=unicodedata.normalize(self._normalisation_standard,chunk_match)

            #If line isn't empty:			
            if chunk_match:

                #Stocking the content of each line in a dictionary. This way we're going to correct line by line later(see function SimpleAltoLoader.replace_content())
                self._dict_alto_lines[line_id]=chunk_match

            #if line is empty:
            else:

                #Stocking line content depending on attribute '_empty_lines_content':
                self._dict_alto_lines[line_id]=chunk_match=self._empty_lines_content

            #Stocking the mapping of character indices to alto lines in the corresponding dictionary
            for char in chunk_match:
                self._char_to_line_map[char_counter]=line_id
                char_counter+=1

            #Appending the corresponding text chunks according to their respective indices and names
            chunk_length=len(chunk_match)
            extracted_text+=chunk_match
            if chunk_length!=0:
                text_chunks.append(TextChunk(list(range(start_chunk_idx, start_chunk_idx+chunk_length)), f"{line_id}"))
            
                #the incrementation can be outside the conditional if , however I kept it inside. no difference.
                start_chunk_idx += chunk_length

        #Loader output
        self.__input_text=extracted_text
        self._output=np.array([ord(x) for x in self.__input_text])
        self._input_output_map=[(x,x) for x in range(0,self.output.size)]
        self._text_chunk_indices=text_chunks

        return super()._load()




    def update_content(self, alignment_pairs: TextAlignments, query_text_chunks: List[TextChunk], target_text_chunks: List[TextChunk],ground_truth_text:LetterList, unaligned_ground_truth_chunks: List[TextChunk] ,interlinears=True):
        """Replace the content of each transcription line in the alto file by the text to which it has been aligned by the text alignment tool. 
            We're using the alignment on a character level to retrieve the aligned chunks and update the alto lines' content.
            So the aligned chunks are obtained indirectly from the final alignment on a character level.

                ####Example usage:####

                ===========================================================================
                Let's say you have "alto_query.xml" stored locally (the alto containing the rough HTR transcription) , as well as the target you want to align to.Let's say the target is for example another
                alto (the target could also be a text using the TxTLoader or a papyrus transcription use the EpidocScraper etc . In principle the target
                text could have been loader).

                ==========================================================================

                #loading query text(rough transcription, pk is the pk of the Escriptorium document containing the image transcribed in the query alto.
                It is not necessary to precise a pk it unless you 're planning to upload it back to Escriptorium as a separate transcription layer using 
                the upload_alto() function , see further into the code for example usages.)

                loader_alto_query=AltoFileLoader("alto_query.xml", pk=1132)


                ===========================================================================

                #loading target text

                loader_alto_target=AltoFileLoader("alto1.xml")

                ===========================================================================

                #instanciating a TextAlignmentTool. the query loader is the query, the target loader is the target:

                align=TextAlignmentTool(loader_alto_query, loader_alto_target)

                ===========================================================================
                
                #Instanciating a chunk alignment algorithm:

                algo_1=ChunkAlignmentJaroWinkler()

                ============================================================================
                
                #aligning

                align.align_text(algo_1)

                ============================================================================
                
                #retrieving the query chunks and the target chunks. We'll need them to pass them as a parameter in the update_lines function.

                query_chunks=align.latest_alignment.input_query_text_chunk_indices
                target_chunks=align.latest_alignment.input_target_text_chunk_indices

                ============================================================================
                #Get the chunk map prior to calling the function with the get_chunk_map property of the alignment algorithm instance you've used to align
                #your source(alto) to your target(reference transcription).
                #retrieving the chunk mapping (basically the mapping of the aligned chunks):

                chunk_map=algo_1.get_chunk_map

                ============================================================================
                
                #Retrieving the alignment pairs and the unaligned chunks to pass them as argument in the update_content() function:				

                alignment_pairs=align.latest_alignment.input_output_alignment

                unaligned=align.latest_alignment_unaligned_chunks

                ============================================================================
                
                #Retrieving the text of the last ground truth (last target text):

                ground_truth=align.latest_alignment_target
                
                ============================================================================
                #Updating the alto content

                loader_alto_query.update_content(alignment_pairs=alignment_pairs, query_text_chunks= query_chunks, target_text_chunks= target_chunks, ground_truth_text=ground_truth, unaligned_ground_truth_chunks=unaligned)

                ============================================================================

                #Checking the updated lines/comparing them to the original lines' content:

                print(loader_alto_query.show_alto_lines_updated)
                print(loader_alto_query.show_alto_lines)

                ============================================================================
                
                #Updating the query alto file using function update_file() (see further into the code):

                loader_alto_query.update_file()

                ============================================================================

            
        """

        aligned_text_chunks : set [tuple[int, int]]=set([])
        
        #Getting the aligned chunks from the alignment indices that the algorithm being used yields as output:
        alignments_idx=0
        while (alignments_idx is not None and alignments_idx<len(alignment_pairs.alignments)):

            current_alignment=alignment_pairs.alignments[alignments_idx]
            stop_query= False
            for q_idx, query_text_chunk in enumerate(query_text_chunks):
                for t_idx, target_text_chunk in enumerate(target_text_chunks):
                    
                    if(query_text_chunk.indices[0]<=current_alignment.query_idx<=query_text_chunk.indices[-1] and target_text_chunk.indices[0]<=current_alignment.target_idx<=target_text_chunk.indices[-1]):

                        aligned_text_chunks.add((q_idx,t_idx))
                        stop_query=True
                        break
                if stop_query:
                    break
            alignments_idx +=1

        #Updating the dictionary containing the content of the lines based on the aligned_text_chunks:
        for aligned_text_chunk in aligned_text_chunks:
            query_text_chunk= query_text_chunks[aligned_text_chunk[0]]
            target_text_chunk=target_text_chunks[aligned_text_chunk[1]]
            
            line_id=query_text_chunk.name
            new_text="".join([chr(x) for x in ground_truth_text[target_text_chunk.indices[0]: target_text_chunk.indices[-1]+1]])

            #if the function is called with an interlinear parameter being equal to False (by default the said parameter is True):
            if not interlinears:
                new_text=new_text.replace("{", "")
                new_text=new_text.replace("}","")
            self._dict_alto_lines_updated[line_id]=new_text	
        
                
        #We're going to append all the unaligned text chunks to the last line. It's the best way to keep whatever text stays unaligned and upload it to eScriptorium without altering any original annotation to the image.
        unaligned_chunks_aggregated=""
        aligned_ground_truth_chunks=[x[1] for x in aligned_text_chunks]
        for idx,text_chunk in enumerate(unaligned_ground_truth_chunks):
            text="".join([chr(ground_truth_text[y]) for y in range(text_chunk.indices[0], text_chunk.indices[-1]+1)])
            text=f"(start dummy line{idx}){text}(end dummy line{idx})"
            unaligned_chunks_aggregated+=text

        
        #appending all the unaligned chunks' text to the content of the last line
        if self._last_line_id in self._dict_alto_lines_updated and unaligned_chunks_aggregated!="":
            self._dict_alto_lines_updated[self._last_line_id]=f"(start_last_aligned_line){self._dict_alto_lines_updated[self._last_line_id]}(end_last_aligned_line) {unaligned_chunks_aggregated}"
        elif unaligned_chunks_aggregated != "":

            #Avoiding KeyError:
            self._dict_alto_lines_updated[self._last_line_id]=unaligned_chunks_aggregated

            #Updating content:
            self._dict_alto_lines_updated[self._last_line_id]=f"(start_last_aligned_line){self._dict_alto_lines_updated[self._last_line_id]}(end_last_aligned_line){unaligned_chunks_aggregated}"
        elif self._last_line_id not in self._dict_alto_lines_updated:
            self._dict_alto_lines_updated[self._last_line_id] = self._dict_alto_lines[self._last_line_id]

        
        
        return self._dict_alto_lines_updated
        
        
        
    def update_lines(self, chunk_map, query_text_chunks: List[TextChunk], target_text_chunks: List[TextChunk], ground_truth_text:LetterList, interlinears=True):
        """
        Replace the content of each line by the text to which it has been aligned by the text alignment tool. 
        We're using the alignment on a line level to update the alto lines.	Here, the aligned chunk pairs are retrieved directly from the raw output 
        of the alignment algorithm.		
                
                ####Example Usage:####

                Let's say you have alto_query stored locally , as well as the target you want to align to.Let's say the target is for example another
                alto (the target could also be a text using the TxTLoader or a papyrus transcription use the EpidocScraper etc .

                ===========================================================================

                #loading query (rough transcription, pk is the pk of the Escriptorium document containing the image transcribed in the query alto.
                It is not necessary to precise it unless you want to upload it back to Escriptorium as a separate transcription layer using the upload_alto()
                function , see further into the code for example usages.)

                loader_alto_query=AltoFileLoader("alto_query.xml", pk=1132)


                ===========================================================================

                #loading target

                loader_alto_target=AltoFileLoader("alto1.xml")

                ===========================================================================

                #instanciating a TextAlignmentTool. the query loader is the query, the target loader is the target:

                align=TextAlignmentTool(loader_alto_query, loader_alto_target)

                ===========================================================================
                
                #Instanciating a chunk alignment algorithm, for example the Jaro-Winkler version of the chunk alignment.

                algo_1=ChunkAlignmentJaroWinkler()

                ============================================================================
                
                #aligning

                align.align_text(algo_1)

                ============================================================================
                
                #retrieving the query chunks and the target chunks. We'll need them to pass them as a parameter in the update_lines function.

                query_chunks = align.latest_alignment.input_query_text_chunk_indices
                target_chunks = align.latest_alignment.input_target_text_chunk_indices

                ============================================================================

                #Get the chunk map prior to calling the function with the get_chunk_map property of the alignment algorithm instance you've used to align
                #your source(alto) to your target(reference transcription).

                #Retrieving the chunk mapping (which is the mapping of the aligned chunks):

                chunk_map = algo_1.get_chunk_map

                ============================================================================
                
                #Retrieving the text of the last ground truth (target text):

                ground_truth = align.latest_alignment_target
                
                ============================================================================
                #Updating the lines

                loader_alto_query.update_lines(chunk_map=chunk_map, query_text_chunks= query_chunks, target_text_chunks= target_chunks, ground_truth_text=ground_truth)

                ============================================================================
                #Checking the updated lines/comparing them to the original lines' content:

                print(loader_alto_query.show_alto_lines_updated)

                print(loader_alto_query.show_alto_lines)

                ============================================================================
                
                #Updating the query alto file using function update_file() (see further into the code):

                loader_alto_query.update_file()

                =============================================================================			

        """
            
        #Using the raw output of the aligned chunks to get the content replaced.
        for aligned_chunk_couple in chunk_map:
            query_text_chunk= aligned_chunk_couple[0]
            target_text_chunk= aligned_chunk_couple[1]
            
            line_id=query_text_chunk.name
            new_text="".join([chr(x) for x in ground_truth_text[target_text_chunk.indices[0]: target_text_chunk.indices[-1]+1]])
            
            #if the function is called with an interlinear parameter being equal to False (by default the said parameter is True):
            if not interlinears:
                new_text=new_text.replace("{", "")
                new_text=new_text.replace("}","")
            self._dict_alto_lines_updated[line_id]=new_text
        

        
        #We're going to append all the unaligned text chunks to the last line. It's the best way to keep whatever text stays unaligned and upload it to eScriptorium without altering any original annotation to the image.
        
        unaligned_chunks_aggregated= ""
        aligned_ground_truth_chunks=[x[1] for x in chunk_map]
        
        unaligned_ground_truth_chunks=[x for x in target_text_chunks if x not in [y[1] for y in chunk_map]]

        #Updating the value of the content of the last line 
        for idx,text_chunk in enumerate(unaligned_ground_truth_chunks):
            text="".join([chr(ground_truth_text[y]) for y in range(text_chunk.indices[0], text_chunk.indices[-1]+1)])
            text=f"(start dummy line{idx+1}){text}(end dummy line{idx+1})"
            unaligned_chunks_aggregated+=text
            
        #appending all the unaligned chunks' text to the content of the last line
        if self._last_line_id in self._dict_alto_lines_updated and unaligned_chunks_aggregated!="":
            self._dict_alto_lines_updated[self._last_line_id]=f"(start_last_aligned_line){self._dict_alto_lines_updated[self._last_line_id]}(end_last_aligned_line) {unaligned_chunks_aggregated}"
        elif unaligned_chunks_aggregated != "":

            #Avoiding KeyError:
            self._dict_alto_lines_updated[self._last_line_id]=unaligned_chunks_aggregated

            #Updating content:
            self._dict_alto_lines_updated[self._last_line_id]=f"(start_last_aligned_line){self._dict_alto_lines_updated[self._last_line_id]}(end_last_aligned_line){unaligned_chunks_aggregated}"
        elif self._last_line_id not in self._dict_alto_lines_updated:
            self._dict_alto_lines_updated[self._last_line_id] = self._dict_alto_lines[self._last_line_id]
        


        return self._dict_alto_lines_updated

    
    def update_file(self):
    
        """Update the lines in the actual alto. 
            This basically creates the new alto that will be uploaded to eScriptorium. It also saves the transcription locally.
        """
        
        tree_2=et.parse(self._file_path)
        root_2=tree_2.getroot()
        namespace='{http://www.loc.gov/standards/alto/ns-v4#}'
        line_element="TextLine"
        content_element='String'
        line_query='.//'+namespace+line_element
        text_query=namespace+content_element
        for line in root_2.findall(line_query):
            line_id=line.attrib.get("ID")
            content=line.find(text_query)
            content_updated=self._dict_alto_lines_updated.get(line_id,"")
            content.set("CONTENT",content_updated)
        with open(self._file_path, "wb") as f:
            et.register_namespace("","http://www.loc.gov/standards/alto/ns-v4#")
            tree_2.write(f, encoding="utf-8")		
            
            
                
    def upload(self, user, password):
        """Upload alto file back to eScriptorium as a separate transcription layer.Make sure to upload only after updating the alto lines. 
        pass your username and password as arguments.
        example, after having aligned and updated the file (see above), run:
        ===============================================================================================
        #assuming your query loader is named `loader_alto_query`. It could have any name by the way:

        loader_alto_query.upload(user="userexample",password="1234")

        """

        url = "https://msia.escriptorium.fr/"
        escriptorium_connector=EscriptoriumConnector(url,user,password)
        file=self._file_path
        with open(file,"rb") as alto_to_upload:
            file_data=io.BytesIO(alto_to_upload.read())
        escriptorium_connector.upload_part_transcription(self._document_pk,transcription_name=f"alto_upload_{str(user)}", filename=self._file_path, file_data= file_data)
            
    def save_copy(self, dir="copied_alto_files"):
        """Save a local copy of the new alto file containing the aligned content.Make sure to save only after the replacement with the aligned transcription text has taken place. The original alto file has already been modified
        by the update_alto() function, but you can save another copy using this function.In path and dir it's better to pass the absolute paths otherwise you're starting from the directory you're in.
        """

        alto_path=self._file_name
        current=os.getcwd()		
        if not os.isdir(dir):
            os.mkdir(dir)
        destination_path=os.path.join(dir,self._file_name)
        shutil.copy2(alto_path,destination_path)		
        
    
    #Use below properties to visualize results in the alto lines dict_alto_lines_updated and compare it to initial line content , i.e. dict_alto_lines.   	
    @property
    def show_alto_lines_updated(self):	
        return self._dict_alto_lines_updated

    @property
    def show_alto_lines(self):
        return self._dict_alto_lines

        