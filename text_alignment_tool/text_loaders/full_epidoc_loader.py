#The purpose here is to scrape the whole transcription and then perform a chunk alignment with the rough OCR transcription.
#Hypothetically we can alternatively align locally each line of the rough OCR transcription with the whole text of the papyrus.
#Then an additional global alignment would possibly be required for each line in order to adjust the alignment even more.
#Each missing caracter is replaced by qn underscore`_`. Otherwise the user can change the "missing attribute in the class, for example choose to 
#replace by an underscore `_`.
#Lines missing from the original can be included or not. The default attribute is for them to not be included.
#In this loader, we do not remove the <supplied>, the <g> and the <ex>. However we do remove the <reg> , the <rdg>, the <corr> and the <figure>.
#We also keep all the diacritics, regardless of whether they're included in the original or not.

#!!!The FullEpidocScraper is not up to date compared to the much better EpidocScraper and PapyrusLoader and is too simplified. Use at your own risk.


from typing import List
from text_alignment_tool.shared_classes import TextChunk, LetterList
from text_alignment_tool.text_loaders.text_loader import TextLoader
import numpy as np
import requests
from urllib.request import urlopen
import lxml.etree as et
from unicodedata import normalize


class FullEpidocScraper(TextLoader):


	def __init__(self,tm_number, missing="_", norm="NFC", include=False):

		super().__init__()
		self._tm_number=tm_number
		self._missing_character=missing
		self._include_gap_lines=include	
		#Modify the following parameter if you need NFD, NFKC or NFKD.If you do not set a normalization standard then NFC is set by default.
		self._normalisation_standard=norm
		self.__input_text=""

	def _load(self) -> LetterList:
		epidoc_final_text=""
		text_chunks=[]
		start_chunk_idx=0
		#The numbering of the lines will be internal to the loader we choose not to keep the @n attribute of the transcription
		#We're not interested in the original numbering of the lines in the TEI
		chunk_count=0	
		#The transcription of the papyrus in question will be accessible in either one of the two datatabases (ddbdp or dclp). 
		#The papyroLogos project deals mainly with those two. If we're dealing with one of the DCLP papyri, the transcription
		#can be directly accessed using the TM. If however we're dealing with a DDBDP papyrus, we first need to access its metadata
		#which are on the HGV database, and then use that metadata(in this case the id) in order to access the url 
		#containing the epidoc of the transcription.
		#DCLP url:
		address_a='https://papyri.info/dclp/'
		#HGV url(this database contains metadata concerning DDBDP papyri):
		address_b='https://papyri.info/hgv/'
		#DDBDP url:
		address_c='https://papyri.info/ddbdp/'
		#We'll need those below to add them to the basic url above and make up the full url we're looking for, depending on the papyrus in question:
		address_part_2=str(self._tm_number)
		address_part_3='/source'
		#Checking if we're in the simple scenario (papyrus part of DCLP project)
		url_1=address_a+address_part_2+address_part_3
		if requests.get(url_1).status_code==200 or requests.get(url_1).status_code==201:
			url=url_1
		
			#If on the other hand it's a DDBDP papyrus, we first need to access the HGV database using its TM number, extract its ID from the Epidoc, and then use that ID 
		#to access the Epidoc of the transcription on DDBDP
		else:
			metadata_url=address_b+address_part_2+address_part_3
			with urlopen(metadata_url) as f:
				metadata_tree=et.parse(f)
				root_metadata=metadata_tree.getroot()
			papyrus_id=root_metadata.find(".//{http://www.tei-c.org/ns/1.0}idno[@type='ddb-hybrid']").text
			#Let's clean the text of the <idno type='ddb-hybrid'> tag in case any undesired extra spaces are present.
			papyrus_id=papyrus_id.strip()
			url=address_c+papyrus_id+address_part_3
		with urlopen(url) as f:
			tree=et.parse(f)
			root=tree.getroot()
		
			#Clean the elements' text from unnecessary spaces before and after the text content:
		for element_to_clean in root.findall('.//{http://www.tei-c.org/ns/1.0}*'):
			if element_to_clean.text:
				element_to_clean.text=element_to_clean.text.strip() 
			if element_to_clean.tail:
				element_to_clean.tail=element_to_clean.tail.strip() 	
		for element_to_del in root.findall('.//{http://www.tei-c.org/ns/1.0}reg'):
			for element in element_to_del.getchildren():
				element.getparent().remove(element)
			element_to_del.text=""
		
			#TEI namespace			
		namespace='{http://www.tei-c.org/ns/1.0}'
		
		#xpath query to look for transcription text later
		query=".//"+namespace+"div[@corresp]"
		query_alternative_1=".//"+namespace+"div[@type='edition']"
		
		#If we can't find any of these types of divs we just look for
		query_alternative_2=".//"+namespace+"text//"+namespace+"div"			
		
		#We now add linebreaks properly following the <lb> element.
		#As a matter of fact are detected as text nodes ("\n") by the parser each time a new line is started in the page. However we add our own linebreaks
		#this way we're sure the <lb> in the TEI will become a linebreak. We use \r to differentiate from \n (which are already present)
		for rule in root.findall('.//{http://www.tei-c.org/ns/1.0}milestone[@rend="horizontal-rule"]'):
			rule.text="--------"
		for line in root.findall('.//{http://www.tei-c.org/ns/1.0}lb'):
			line.text="\r"
		if self._include_gap_lines:
			for missing_line in root.findall(".//{http://www.tei-c.org/ns/1.0}gap[@unit='line']"):
				n_lines=int(missing_line.get("quantity")) if missing_character.get("quantity") else 1
				for i in range(n_lines):
					missing_line.text="-- -- -- -- -- -- -- -- -- --"
		
					#What we need to process next are the missing characters:
		for missing_character in root.findall(".//{http://www.tei-c.org/ns/1.0}gap[@unit='character']"):
			n_characters=int(missing_character.get("quantity")) if missing_character.get("quantity") else 1
			for i in range(n_characters):
				missing_character.text=self._missing_character				
				#missing_character.text='_'
		
		#Now we're finally going to look for the text of the transcription. In addition to that text we'll have
		#The @type of the divs containing the text of the transcription are not consistent among papyri. Sometimes we have an @corresp, other 
		#papyri have an @type="edition" etc.
		text_query=query if len(root.findall(query)) else (query_alternative_1 if len(root.findall(query_alternative_1)) else  query_alternative_2)
		if len(root.findall(text_query)):
			for div in root.findall(text_query):
				joined_text=""
				text_matches=div.xpath('.//text()')
				space_list=["  ","   ","    ","     "]
				for text_match in text_matches:
					joined_text=text_match
					epidoc_final_text+=joined_text
			epidoc_final_text=epidoc_final_text.replace("\n","")
			epidoc_final_text=normalize(self._normalisation_standard,epidoc_final_text)
			
			#double and triple spaces, if any, transformed to simple space:
			for i in space_list:
				epidoc_final_text=epidoc_final_text.replace(i," ")							
			self.__input_text=epidoc_final_text
			for chunk_match in self.__input_text.split("\r"):
				chunk_length=len(chunk_match)
				#if line is not empty (in the sense of a real empty line intended to be empty, and not a missing line in the sense of a <gap>):
				if chunk_length!=0:
					text_chunks.append(TextChunk(list(range(start_chunk_idx,start_chunk_idx+chunk_length)), f" line {chunk_count}"))
					chunk_count+=1
				else:
					chunk_count+=1
				start_chunk_idx += chunk_length

		#It could be that the papyrus is simply not transcribed. In that case:
		else:
			self.__input_text=""		
		self._output=np.array([ord(x) for x in self.__input_text if x!="\r"])
		self._input_output_map=[(x,x) for x in range(0,self.output.size)]
		self._text_chunk_indices=text_chunks

		return super()._load()