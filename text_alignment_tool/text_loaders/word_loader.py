#Use it to load texts from MS Word Documents. This loader only works for the files with the recent extension .docx .

import os
import sys
from zipfile import ZipFile as zip
from lxml.etree import parse
import numpy as np
from typing import List, Dict, Tuple
from text_alignment_tool.shared_classes import TextChunk, LetterList
from text_alignment_tool.text_loaders.text_loader import TextLoader


class WordTextLoader(TextLoader):

	def __init__(self, file_path):
		super().__init__(file_path)
		self.__input_text=""

	def _load(self):
		extracted_text=""
		text_chunks=[]
		#word counter in the text 
		word_idx=0
		#TextChunk start index cursor.
		start_idx=0
		#TextChunk end index cursor.We start at -1 because we later use a while loop that increments by 1 right from the first iteration.
		end_idx=-1
		#Default namespace is not always the same in the word xml so we use {*}
		name_space='{*}'
		#Unzip the word file:
		with zip(self._file_path, 'r') as zipped_folder:
			xml_tree=parse(zipped_folder.open('word/document.xml', mode='r'))
			root=xml_tree.getroot()
		#Look for all <t> nodes, t as in text.
		xpath_query='.//'+name_space+'t'
		list_of_nodes=root.findall(xpath_query)
		#Go through all the text nodes in the word/xml
		for node in list_of_nodes:
				node_text=node.text
				#Replacing multiple-space occurrences by a single space:
				for idx, char in enumerate(node_text):
					idx_following=idx+1
					if char==" ":
						while len(node_text)>idx_following and node_text[idx_following]==' ':				
							node_text=node_text[0 : idx_following] + node_text[idx+2 : :]
				extracted_text+=node_text
				#Chunking the text into words/tokens: spaces are not included, therefore a later reconstruction of the text based solely on the text chunks and not on the output would require
				#adding the spaces in between. The name of  the text chunk is the index of the word within the entire word document.				
				#We basically add a new text chunk everytime we encounter a space or when we reach the end of a node:
				
				#TextChunk end index cursor.We start at -1 because we later use a while loop that increments by 1 right from the first iteration.
				node_text_counter=-1	
				while node_text_counter<len(node_text)-1:
					end_idx += 1	
					node_text_counter += 1
					
					if  node_text==" ":
						start_idx+=1
					elif (node_text[node_text_counter]=="\r" or node_text[node_text_counter]==" ") and node_text_counter>=1 and node_text[node_text_counter-1]!="\r" and node_text[node_text_counter-1]!=" ":
						text_chunks.append(TextChunk(list(range(start_idx,end_idx)),str(word_idx)))
						word_idx += 1
						start_idx=end_idx
						#Moving start_idx one char forward:
						start_idx += 1
					elif node_text_counter==len(node_text)-1:	
						
						text_chunks.append(TextChunk(list(range(start_idx,end_idx+1)),str(word_idx)))
						word_idx += 1		
						start_idx=end_idx
						start_idx += 1								
					elif (node_text[node_text_counter]==' ' or node_text[node_text_counter]=='\r'):
						
						start_idx+=1	
		self.__input_text=extracted_text
		self._output=np.array([ord(x) for x in self.__input_text])
		self._text_chunk_indices=text_chunks
		self._input_output_map=[(x,x) for x in range(0,self.output.size)]
		return super()._load()
		
