from typing import List, Tuple
from text_alignment_tool.shared_classes import LetterList, TextChunk
from text_alignment_tool.text_transformers import TextTransformer
import numpy as np


class Capitalize(TextTransformer):
	def __init__(self):
		super().__init__()

	def transform(self) -> LetterList:
		if self._output.size > 0:
			return self._output

		self.__transform()

		return super().transform()

	def __transform(self):
		"""Capitalize all characters in the source or the target
			text. Will mainly be used to capitalize the text of the official transcription(target).
			You should first normalize and only then should you capitalize since you might lose some of the diacriticals if they're aggregated with the character.
        	"""

		input_output_map: List[Tuple[int, int]] = []

		output: List[int] = []
		output_chunks: List[TextChunk] = []
		for chunk in self._input_text_chunk_indices:
			current_chunk_indices: List[int] = []
			for idx in chunk.indices:
				character = self.input[idx]
				capitalized = chr(character).upper()
				for char in capitalized: 
					output.append(ord(char))
					current_char_index = len(output) - 1
					current_chunk_indices.append(current_char_index)
					input_output_map.append((idx, current_char_index))

			if current_chunk_indices:
				output_chunks.append(TextChunk(current_chunk_indices, chunk.name))

		self._input_output_map = input_output_map
		self._output = np.array(output, dtype=np.uint32)
		self._text_chunk_indices = output_chunks
