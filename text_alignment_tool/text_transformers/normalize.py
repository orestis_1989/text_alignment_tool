from typing import List, Tuple
from text_alignment_tool.shared_classes import LetterList, TextChunk
from text_alignment_tool.text_transformers import TextTransformer
import numpy as np
from unicodedata import normalize


class Normalize(TextTransformer):
	def __init__(self, norm="NFD"):
		super().__init__()
		#Choose your normalisation standard when instanciating the loader.Default is NFD.Other possible values are NFC, NFKC, NFKD.
		self._normalisation_standard=norm

	def transform(self) -> LetterList:
		if self._output.size > 0:
			return self._output

		self.__transform()

		return super().transform()

	def __transform(self):
		"""Normalize to NFC, NFD, NFKC or NFKD depending on attribute 'norm' chosen when instanciating the transformer.
		"""

		input_output_map: List[Tuple[int, int]] = []

		output: List[int] = []
		output_chunks: List[TextChunk] = []
		for chunk in self._input_text_chunk_indices:
			current_chunk_indices: List[int] = []

			#String in the input text that corresponds to the chunk being processed/normalised
			string_corresponding_to_chunk=""

			#Normalizing the string
			for idx in chunk.indices:
				char = self.input[idx]
				string_corresponding_to_chunk+=chr(char)
			string_normalised=normalize(self._normalisation_standard,string_corresponding_to_chunk)

			#Appending the normalised string as character by character, as unicode codes, to the output of the transformer
			for char in string_normalised:
				output.append(ord(char))
				current_char_index=len(output)-1
				current_chunk_indices.append(current_char_index)
				input_output_map.append((idx,current_char_index))

			#Appending the new TextChunk to the output of the transformer
			if current_chunk_indices:
				output_chunks.append(TextChunk(current_chunk_indices, chunk.name))
		self._input_output_map = input_output_map
		self._output = np.array(output, dtype=np.uint32)
		self._text_chunk_indices = output_chunks
