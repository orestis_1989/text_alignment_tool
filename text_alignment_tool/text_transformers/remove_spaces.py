from typing import List, Tuple
from text_alignment_tool.shared_classes import LetterList, TextChunk
from text_alignment_tool.text_transformers import TextTransformer
import numpy as np


class RemoveSpaces(TextTransformer):
	def __init__(self):
		super().__init__()

	def transform(self) -> LetterList:
		if self._output.size > 0:
			return self._output

		self.__transform()

		return super().transform()

	def __transform(self):
		"""Remove spaces in the source or the target text. Useful for Ancient Greek where no spaces are present in the original.
		"""

		input_output_map: List[Tuple[int, int]] = []

		output: List[int] = []
		output_chunks: List[TextChunk] = []
		space_character=ord(' ')
		for chunk in self._input_text_chunk_indices:
			current_chunk_indices: List[int] = []
			for idx in chunk.indices:
				char = self.input[idx]
				if char==space_character:
					continue
				output.append(char)
				current_char_index = len(output) - 1
				current_chunk_indices.append(current_char_index)
				input_output_map.append((idx, current_char_index))

			if current_chunk_indices:
				output_chunks.append(TextChunk(current_chunk_indices, chunk.name))

		self._input_output_map = input_output_map
		self._output = np.array(output, dtype=np.uint32)
		self._text_chunk_indices = output_chunks
