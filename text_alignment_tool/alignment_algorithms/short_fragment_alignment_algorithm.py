from text_alignment_tool.alignment_algorithms import (
    AlignmentException,
)
import edlib
from text_alignment_tool.alignment_algorithms import (
    AlignmentAlgorithm,
    global_alignment,
)
from text_alignment_tool.shared_classes import (
    TextChunk,
    LetterList,
    TextAlignments,
    AlignmentKey,
)
from typing import List, Dict, Tuple
from text_alignment_tool.alignment_algorithms import AlignmentAlgorithm
from statistics import mean
from fuzzywuzzy import fuzz
import swalign
from collections import Counter
from itertools import combinations
from itertools import permutations
from itertools import product
from itertools import combinations_with_replacement
import logging
import math
from jellyfish import levenshtein_distance


class ShortFragmentAlignment(AlignmentAlgorithm):
    """ShortFragmentsAlignment analyzes the query and the target text
    In this variant of the Chunk Alignment algorithm you ***always need to load the query (HTR)***  as the first attribute in the aligner instance and not the inverse otherwise
    the result will be irrelevant. For example instanciate like this : aligner_1=TextAlignmentTool(query, target).
    Use this algorithm for short texts where query and target have no more than 5 lines each. 
    """

    def __init__(self):
        super().__init__()
        self._output_chunks=[]
        self._unaligned_chunks=[]

    def align(self) -> TextAlignments:
        if self._alignment.alignments:
            return self._alignment

        linewise_matches = comprehensive_linewise_matching(
            self._query,
            self._target,
            self._input_query_text_chunk_indices,
            self._input_target_text_chunk_indices,
        )

        # Find best matches for lines
        alignment = TextAlignments()
        linewise_dict: Dict[int, list[int]] = {}
        for linewise_match in linewise_matches:
            if linewise_match[0] not in linewise_dict:
                linewise_dict[linewise_match[0]] = []
            linewise_dict[linewise_match[0]].append(linewise_match[1])

        # Calculate the actual corresponding chunks of the lines
        query_target_text_chunk_alignment: List[Tuple[TextChunk, TextChunk]] = []
        for linewise_match_key in linewise_dict.keys():
            query_text_chunk_indices = self._input_query_text_chunk_indices[
                linewise_match_key
            ]
            if len(linewise_dict[linewise_match_key]) == 1:
                query_target_text_chunk_alignment.append(
                    (
                        query_text_chunk_indices,
                        self._input_target_text_chunk_indices[
                            linewise_dict[linewise_match_key][0]
                        ],
                    )
                )
                continue

            query_text = self._query[
                query_text_chunk_indices.indices[0] : query_text_chunk_indices.indices[-1]
                + 1
            ]
            for target_line_index in sorted(
                linewise_dict[linewise_match_key],
                key=lambda k: self._input_target_text_chunk_indices[k].indices[-1]
                - self._input_target_text_chunk_indices[k].indices[0],
                reverse=True,
            ):
                target_text_chunk_indices = self._input_target_text_chunk_indices[
                    target_line_index
                ]
                target_text = self._target[
                    target_text_chunk_indices.indices[0]: target_text_chunk_indices.indices[-1]
                    + 1
                ]
                (
                    target_start_idx,
                    target_end_idx,
                    query_start,
                    query_end,
                ) = perform_local_chunk_alignment(query_text, target_text)
                query_target_text_chunk_alignment.append(
                    (
                        TextChunk(list(range(
                            query_text_chunk_indices.indices[0] + query_start,
                            query_text_chunk_indices.indices[-1] + query_end + 1,
                        ))),
                        TextChunk(list(range(
                            target_text_chunk_indices.indices[0] + target_start_idx,
                            target_text_chunk_indices.indices[-1] + target_end_idx+1))
                        ),
                    )
                )
                # Nullify the matched sequence
                section_length = len(query_text[query_start : query_end + 1])
                query_text[query_start : query_end + 1] = [1] * section_length

        #Adding this attribute for the papyroLogos project, it is however not specific to it.
        #We're going to use these aligned chunks to correct the alto of the rough OCR downloaded from e-Scriptorium (papyroLogos project).
        #You can use the aligned text_chunks to replace the content of the "bad OCR" alto file with 
        #with the corresponding respective text chunks of the official transcription that were aligned to each of the "bad" text_chunks.   
        self._output_chunks=query_target_text_chunk_alignment
        self._unaligned_chunks=[x for x in self._input_target_text_chunk_indices if x not in [y[1] for y in self._output_chunks]]  

        # Build also the full alignment (based upon the chunk alignment)        
        for chunk_alignment in query_target_text_chunk_alignment:
            query = self._query[
                chunk_alignment[0].indices[0] : chunk_alignment[0].indices[-1] + 1
            ]
            target = self._target[
                chunk_alignment[1].indices[0] : chunk_alignment[1].indices[-1] + 1
            ]
            if query.size == 0 or target.size == 0:
                continue

            best_alignment = perform_global_alignment(
                query,
                target,
                chunk_alignment[0].indices[0],
                chunk_alignment[1].indices[0],
            )
            alignment.alignments = alignment.alignments + best_alignment

        # self.__query_target_text_chunk_alignment = query_target_text_chunk_alignment
        self._output_query_text_chunk_indices = [
            x[0] for x in query_target_text_chunk_alignment
        ]
        self._output_target_text_chunk_indices = [
            x[1] for x in query_target_text_chunk_alignment
        ]
        self._alignment = alignment
        return self._alignment


def comprehensive_linewise_matching(
    query_text: LetterList,
    target_text: LetterList,
    query_text_chunk_bounds: List[TextChunk],
    target_text_chunk_bounds: List[TextChunk],
) -> List[Tuple[int, int]]:
    query_text_chunks = [
        "".join(chr(y) for y in query_text[x.indices[0] : x.indices[-1] + 1])
        for x in query_text_chunk_bounds
    ]
    
    target_text_chunks = [
        "".join(chr(y) for y in target_text[x.indices[0] : x.indices[-1] + 1])
        for x in target_text_chunk_bounds
    ]
   


   
    
    n_query=len(query_text_chunks)
    n_target=len(target_text_chunks)
    if n_target>5 or n_query>5:
        raise Exception('''The Query Alignment Algorithm is very slow for long texts. It is only suitable for aligning texts composed of up to 5 lines. Please use some other 
        variant of the ChunkAlignmenAlgorithm instead: ChunkAlignmentAlgorithm, ChunkAlignmentLevenshtein, ChunkAlignmentDifflib, ChunkAlignmentJaroWinkler, SimpleChunkAlignmentAlgorithm, LineOrderAlignmentAlgorithm''')
    

    #For the following part to work you need the text chunks to be appended in the right order in the loaders , i.e. in the same order as the order
    #in which they appear in the input text. That is the case for all of the current loaders in the TextAlignMentTool.
    #The code can however be improved to reorder the chunks corrected in case some future loader messes up the chunk order while loading the texts.
    #In this case just sort based on the i.indices[0] for i in query_text_chunk_bounds in the code above and do the same for the target.	
    
    
    
    #Possible combinations (or to be more precise, permutations) of consecutive query lines matching the target lines present in the transcription. For example
    #if the target has 3 lines and the query (meaning, the rough ocr transcription) has 4 lines for some reason
    #( for instance query might have more lines due to one line being split into two in the actual segmentation produced on e-Scriptorium
    #then in this case only 3 of the 4 query lines will be matched to the target.
    lines=n_target
    query_line_numbers=list(range(n_query))
    comb=permutations   
    
    #If target has more lines than query, some query lines will stay unmatched. We therefore add "None" elements to the list of query lines and match the
    #target lines to those "None" elements.
    if n_target>n_query:
        for i in range(n_target-n_query):
            query_line_numbers.append(None)
       
    min_distance=math.inf
    best_order=None
    for line_order_possibility in comb(query_line_numbers, lines):        
        distance=0 
        
        #Target line index is the index of the element in the tuple being considered.
        #Query line index is the value of the element in the tuple being considered.
        for target_line_idx, query_line_idx in enumerate(line_order_possibility): 
            if query_line_idx is not None:
                distance += levenshtein_distance(query_text_chunks[query_line_idx], target_text_chunks[target_line_idx])
                    
        if distance<min_distance:
            min_distance=distance
            best_order=line_order_possibility
    
    return [(y, x) for x,y in enumerate(best_order) if y is not None]


def linewise_matching(
    query_text: LetterList,
    target_text: LetterList,
    query_text_chunk_bounds: List[TextChunk],
    target_text_chunk_bounds: List[TextChunk],
) -> List[Tuple[int, int]]:
    text_alignments: List[Tuple[int, int]] = []
    # The dict is: {query_text_index: [{score: target_text_index}]}
    line_scores: Dict[int, List[Dict[int, int]]] = {}
    query_text_chunks = [
        "".join(chr(y) for y in query_text[x.indices[0] : x.indices[-1]+1])
        for x in query_text_chunk_bounds
    ]
    mean_query_length = mean([len(x) for x in query_text_chunks])
    target_text_chunks = [
        "".join(chr(y) for y in target_text[x.indices[0] : x.indices[-1] + 1])
        for x in target_text_chunk_bounds
    ]
    mean_target_length = mean([len(x) for x in target_text_chunks])

    for target_index, target_text_chunk in enumerate(target_text_chunks):
        for query_index, query_text_chunk in enumerate(query_text_chunks):
            ratio = (
                fuzz.ratio
                if abs(len(query_text_chunk) - len(target_text_chunk))
                > mean_query_length
                else fuzz.partial_ratio
            )
            pair_distance = ratio(query_text_chunk, target_text_chunk)
            if target_index not in line_scores:
                line_scores[target_index] = []
            line_scores[target_index].append({pair_distance: query_index})

    best_matches: list[tuple[int, int, str, int, str]] = []
    unmatched_target_text: list[int] = []
    unmatched_query_text: set[int] = set(range(len(query_text_chunks)))
    for key in line_scores.keys():
        sorted_dict = sorted(line_scores[key], key=lambda l: list(l.keys()))
        sorted_scores = [
            (list(x.keys())[0], list(x.values())[0]) for x in list(sorted_dict)
        ]
        if sorted_scores[-1][0] >= 60:
            best_query_line_index = sorted_scores[-1][1]
            best_matches.append(
                (
                    sorted_scores[-1][0],
                    best_query_line_index,
                    query_text_chunks[best_query_line_index],
                    key,
                    target_text_chunks[key],
                )
            )
            if best_query_line_index in unmatched_query_text:
                unmatched_query_text.remove(best_query_line_index)
            continue
        unmatched_target_text.append(key)

    check_query_line_usage = Counter([x[1] for x in best_matches]).values()

    if not unmatched_query_text and max(check_query_line_usage) == 1:
        return [(x[1], x[3]) for x in best_matches]

    for line_number in unmatched_query_text:
        match_list = []
        query_line_text = query_text_chunks[line_number]
        for target_index, target_text_chunk in enumerate(target_text_chunks):
            match_list.append(
                (
                    fuzz.partial_ratio(query_line_text, target_text_chunk),
                    line_number,
                    target_index,
                )
            )
        sorted_scores = sorted(match_list, key=lambda k: k[0])
        best_query_line_index = sorted_scores[-1][2]
        best_matches.append(
            (
                sorted_scores[-1][0],
                best_query_line_index,
                query_line_text,
                line_number,
                target_text_chunks[best_query_line_index],
            )
        )

    check_query_line_usage = Counter([x[1] for x in best_matches])
    if max(check_query_line_usage.values()) == 1:
        return [(x[1], x[3]) for x in best_matches]

    reused_query_lines = [
        x for x in check_query_line_usage.keys() if check_query_line_usage[x] == 1
    ]
    logging.warning(check_query_line_usage)
    return [(x[1], x[3]) for x in best_matches]


def perform_global_alignment(
    query_text: LetterList,
    target_text: LetterList,
    query_offset: int,
    target_offset: int,
) -> List[AlignmentKey]:
    alignments: List[AlignmentKey] = []
    query = "".join([chr(x) for x in query_text])
    target = "".join([chr(x) for x in target_text])

    result = edlib.align(query, target, task="path", mode="HW")
    nice = edlib.getNiceAlignment(result, query, target)
    query_count_idx = query_offset
    target_count_idx = target_offset + result["locations"][0][0]
    for idx in range(len(nice["query_aligned"])):
        alignments.append(AlignmentKey(query_count_idx, target_count_idx))
        if nice["query_aligned"][idx] != "-":
            query_count_idx += 1
        #Addition 1/6 debugging IndexError:
        if nice["target_aligned"][idx] != "-" and target_count_idx<len(query):
            target_count_idx += 1

    return alignments


def perform_local_chunk_alignment(
    query_text: LetterList,
    target_text: LetterList,
) -> Tuple[int, int, int, int]:
    # choose your own values here… 2 and -1 are common.
    match = 2
    mismatch = -1
    scoring = swalign.NucleotideScoringMatrix(match, mismatch)

    sw = swalign.LocalAlignment(scoring)  # you can also choose gap penalties, etc...
    target_transcription = "".join([chr(x) for x in target_text])
    query_transcription = "".join([chr(x) for x in query_text])
    alignment = sw.align(target_transcription, query_transcription)
    target_start_idx, target_end_idx, query_start, query_end = (
        alignment.r_pos,
        alignment.r_end,
        alignment.q_pos,
        alignment.q_end,
    )
    alignment.dump()

    return target_start_idx, target_end_idx, query_start, query_end

