from text_alignment_tool.alignment_algorithms import (
    AlignmentException,
)
import edlib
from text_alignment_tool.alignment_algorithms import (
    AlignmentAlgorithm,
    global_alignment,
)
from text_alignment_tool.shared_classes import (
    TextChunk,
    LetterList,
    TextAlignments,
    AlignmentKey,
)
from typing import List, Dict, Tuple
from text_alignment_tool.alignment_algorithms import AlignmentAlgorithm
from statistics import mean
from fuzzywuzzy import fuzz
import swalign
from collections import Counter
import logging
import jellyfish
from difflib import SequenceMatcher


class ChunkAlignmentLevenshtein(AlignmentAlgorithm):
    """Compared to the original version of the ChunkAlignmentAlgorithm it uses the Levenshtein distance instead of the fuzz.ratio score."""

    def __init__(self):
        super().__init__()
        self._output_chunks=[]
        self._unaligned_chunks=[]


    def align(self) -> TextAlignments:
        if self._alignment.alignments:
            return self._alignment

        linewise_matches = comprehensive_linewise_matching(
            self._query,
            self._target,
            self._input_query_text_chunk_indices,
            self._input_target_text_chunk_indices,
        )

        # Find best matches for lines
        alignment = TextAlignments()
        linewise_dict: Dict[int, List[int]] = {}
        for linewise_match in linewise_matches:
            if linewise_match[0] not in linewise_dict:
                linewise_dict[linewise_match[0]] = []
            linewise_dict[linewise_match[0]].append(linewise_match[1])

        # Calculate the actual corresponding chunks of the lines
        query_target_text_chunk_alignment: List[Tuple[TextChunk, TextChunk]] = []
        for linewise_match_key in linewise_dict.keys():
            query_text_chunk_indices = self._input_query_text_chunk_indices[
                linewise_match_key
            ]
            if len(linewise_dict[linewise_match_key]) == 1:
                query_target_text_chunk_alignment.append(
                    (
                        query_text_chunk_indices,
                        self._input_target_text_chunk_indices[
                            linewise_dict[linewise_match_key][0]
                        ],
                    )
                )
                continue

            query_text = self._query[
                query_text_chunk_indices.indices[0] : query_text_chunk_indices.indices[-1]
                + 1
            ]
            for target_line_index in sorted(
                linewise_dict[linewise_match_key],
                key=lambda k: self._input_target_text_chunk_indices[k].indices[-1]
                - self._input_target_text_chunk_indices[k].indices[0],
                reverse=True,
            ):
                target_text_chunk_indices = self._input_target_text_chunk_indices[
                    target_line_index
                ]
                target_text = self._target[
                    target_text_chunk_indices.indices[0]: target_text_chunk_indices.indices[-1]
                    + 1
                ]
                (
                    target_start_idx,
                    target_end_idx,
                    query_start,
                    query_end,
                ) = perform_local_chunk_alignment(query_text, target_text)
                query_target_text_chunk_alignment.append(
                    (
                        TextChunk(
                            query_text_chunk_indices.indices[0] + query_start,
                            query_text_chunk_indices.indices[-1] + query_end + 1,
                        ),
                        TextChunk(
                            target_text_chunk_indices.indices[0] + target_start_idx,
                            target_text_chunk_indices.indices[-1] + target_end_idx+1,
                        ),
                    )
                )
                # Nullify the matched sequence
                section_length = len(query_text[query_start : query_end + 1])
                query_text[query_start : query_end + 1] = [1] * section_length
        
        #Adding this attribute for the papyroLogos project, it is however not specific to it.
        #You can use the aligned text_chunks to replace the content of the "bad OCR" alto file with 
        #with the corresponding respective text chunks of the official transcription that were aligned to each of the "bad" text_chunks. 
        self._output_chunks=query_target_text_chunk_alignment
        self._unaligned_chunks=[x for x in self._input_target_text_chunk_indices if x not in [y[1] for y in self._output_chunks]]  
       


        # Build also the full alignment (based upon the chunk alignment)
        for chunk_alignment in query_target_text_chunk_alignment:
            query = self._query[
                chunk_alignment[0].indices[0] : chunk_alignment[0].indices[-1] + 1
            ]
            target = self._target[
                chunk_alignment[1].indices[0] : chunk_alignment[1].indices[-1] + 1
            ]
            if query.size == 0 or target.size == 0:
                continue

            best_alignment = perform_global_alignment(
                query,
                target,
                chunk_alignment[0].indices[0],
                chunk_alignment[1].indices[0],
            )
            alignment.alignments = alignment.alignments + best_alignment

        
        self._output_query_text_chunk_indices = [
            x[0] for x in query_target_text_chunk_alignment
        ]
        self._output_target_text_chunk_indices = [
            x[1] for x in query_target_text_chunk_alignment
        ]
        self._alignment = alignment
        return self._alignment


def comprehensive_linewise_matching(
    query_text: LetterList,
    target_text: LetterList,
    query_text_chunk_bounds: List[TextChunk],
    target_text_chunk_bounds: List[TextChunk],
) -> List[Tuple[int, int]]:
    query_text_chunks = [
        "".join(chr(y) for y in query_text[x.indices[0] : x.indices[-1] + 1])
        for x in query_text_chunk_bounds
    ]
    mean_query_length = mean([len(x) for x in query_text_chunks])
    target_text_chunks = [
        "".join(chr(y) for y in target_text[x.indices[0] : x.indices[-1] + 1])
        for x in target_text_chunk_bounds
    ]
    mean_target_length = mean([len(x) for x in target_text_chunks])

    scored_matches: List[Tuple[int, int, int]] = []
    for target_index, target_text_chunk in enumerate(target_text_chunks):
        for query_index, query_text_chunk in enumerate(query_text_chunks):
               
                pair_distance =jellyfish.levenshtein_distance(query_text_chunk,target_text_chunk)
                scored_matches.append((pair_distance, query_index, target_index))
                print(pair_distance)
    best_matches: List[Tuple[int, int, int]] = []
    sorted_scored_matches = sorted(scored_matches, key=lambda x: x[0])
    remaining_matches: List[Tuple[int, int, int]] = sorted_scored_matches.copy()
    for match in sorted_scored_matches:
        if match not in remaining_matches:
            continue

        best_matches.append(match)
        remaining_matches = list(
            filter(
                lambda x: x[1] != match[1] and x[2] != match[2],
                remaining_matches,
            )
        )
        print([(x[1], x[2]) for x in sorted(best_matches, key=lambda x: x[1])])
    return [(x[1], x[2]) for x in sorted(best_matches, key=lambda x: x[1])]


def linewise_matching(
    query_text: LetterList,
    target_text: LetterList,
    query_text_chunk_bounds: List[TextChunk],
    target_text_chunk_bounds: List[TextChunk],
) -> List[Tuple[int, int]]:
    text_alignments: List[Tuple[int, int]] = []
    # The dict is: {query_text_index: [{score: target_text_index}]}
    line_scores: Dict[int, List[Dict[int, int]]] = {}
    query_text_chunks = [
        "".join(chr(y) for y in query_text[x.indices[0] : x.indices[-1]+1])
        for x in query_text_chunk_bounds
    ]
    mean_query_length = mean([len(x) for x in query_text_chunks])
    target_text_chunks = [
        "".join(chr(y) for y in target_text[x.indices[0] : x.indices[-1] + 1])
        for x in target_text_chunk_bounds
    ]
    mean_target_length = mean([len(x) for x in target_text_chunks])

    for target_index, target_text_chunk in enumerate(target_text_chunks):
        for query_index, query_text_chunk in enumerate(query_text_chunks):
            seq=SequenceMatcher(None,target_text_chunk,query_text_chunk)
            ratio = (
                seq.ratio()
                if abs(len(query_text_chunk) - len(target_text_chunk))
                > mean_query_length
                else seq.ratio()
            )
            pair_distance = ratio
            if target_index not in line_scores:
                line_scores[target_index] = []
            line_scores[target_index].append({pair_distance: query_index})

    best_matches: List[Tuple[int, int, str, int, str]] = []
    unmatched_target_text: List[int] = []
    unmatched_query_text: set[int] = set(range(len(query_text_chunks)))
    for key in line_scores.keys():
        sorted_dict = sorted(line_scores[key], key=lambda l: list(l.keys()))
        sorted_scores = [
            (list(x.keys())[0], list(x.values())[0]) for x in list(sorted_dict)
        ]
        if sorted_scores[-1][0] >= 60:
            best_query_line_index = sorted_scores[-1][1]
            best_matches.append(
                (
                    sorted_scores[-1][0],
                    best_query_line_index,
                    query_text_chunks[best_query_line_index],
                    key,
                    target_text_chunks[key],
                )
            )
            if best_query_line_index in unmatched_query_text:
                unmatched_query_text.remove(best_query_line_index)
            continue
        unmatched_target_text.append(key)

    check_query_line_usage = Counter([x[1] for x in best_matches]).values()

    if not unmatched_query_text and max(check_query_line_usage) == 1:
        return [(x[1], x[3]) for x in best_matches]

    for line_number in unmatched_query_text:
        match_list = []
        query_line_text = query_text_chunks[line_number]
        for target_index, target_text_chunk in enumerate(target_text_chunks):
            match_list.append(
                (
                    fuzz.partial_ratio(query_line_text, target_text_chunk),
                    line_number,
                    target_index,
                )
            )
        sorted_scores = sorted(match_list, key=lambda k: k[0])
        best_query_line_index = sorted_scores[-1][2]
        best_matches.append(
            (
                sorted_scores[-1][0],
                best_query_line_index,
                query_line_text,
                line_number,
                target_text_chunks[best_query_line_index],
            )
        )

    check_query_line_usage = Counter([x[1] for x in best_matches])
    if max(check_query_line_usage.values()) == 1:
        return [(x[1], x[3]) for x in best_matches]

    reused_query_lines = [
        x for x in check_query_line_usage.keys() if check_query_line_usage[x] == 1
    ]
    logging.warning(check_query_line_usage)
    return [(x[1], x[3]) for x in best_matches]


def perform_global_alignment(
    query_text: LetterList,
    target_text: LetterList,
    query_offset: int,
    target_offset: int,
) -> List[AlignmentKey]:
    alignments: List[AlignmentKey] = []
    query = "".join([chr(x) for x in query_text])
    target = "".join([chr(x) for x in target_text])

    result = edlib.align(query, target, task="path", mode="HW")
    nice = edlib.getNiceAlignment(result, query, target)
    query_count_idx = query_offset
    target_count_idx = target_offset + result["locations"][0][0]
    for idx in range(len(nice["query_aligned"])):
        alignments.append(AlignmentKey(query_count_idx, target_count_idx))
        if nice["query_aligned"][idx] != "-":
            query_count_idx += 1
        #Addition here 1/6 debugging IndexError out of bounds:
        if nice["target_aligned"][idx] != "-" and target_count_idx<len(target):
            target_count_idx += 1

    return alignments


def perform_local_chunk_alignment(
    query_text: LetterList,
    target_text: LetterList,
) -> Tuple[int, int, int, int]:
    # choose your own values here… 2 and -1 are common.
    match = 2
    mismatch = -1
    scoring = swalign.NucleotideScoringMatrix(match, mismatch)

    sw = swalign.LocalAlignment(scoring)  # you can also choose gap penalties, etc...
    target_transcription = "".join([chr(x) for x in target_text])
    query_transcription = "".join([chr(x) for x in query_text])
    alignment = sw.align(target_transcription, query_transcription)
    target_start_idx, target_end_idx, query_start, query_end = (
        alignment.r_pos,
        alignment.r_end,
        alignment.q_pos,
        alignment.q_end,
    )
    alignment.dump()

    return target_start_idx, target_end_idx, query_start, query_end

