from dataclasses import dataclass
from typing import List, Dict, Tuple
from text_alignment_tool.shared_classes import (
    LetterList,    
    TextAlignments,
    AlignmentKey,
    TextChunk
)

from text_alignment_tool.analyzers import get_text_chunk_string

from text_alignment_tool.alignment_algorithms import AlignmentAlgorithm
import minineedle
from minineedle import needle
from fuzzywuzzy import fuzz
import numpy as np
from scipy.optimize import linear_sum_assignment


class LineOrderAlignmentAlgorithm(AlignmentAlgorithm):
    """Aligns two texts based upon the chunking of the texts.
    It first tries to align the texts, by finding the most
    successful match based on the designated line ordering.
    Then it makes alignment decisions based upon text similarity.
"""

    def __init__(self):
        super().__init__()
        self._input_query_text_chunk_indices: List[TextChunk] = []
        self._input_target_text_chunk_indices: List[TextChunk] = []
        self.__aligned_text_chunks: List[Tuple[TextChunk, TextChunk]] = []
        self._output_chunks=[]
        self._unaligned_chunks=[]


    @property
    def aligned_text_chunks(self) -> List[Tuple[TextChunk, TextChunk]]:
        if not self._aligned_text_chunks:
            self.align()

        return self._aligned_text_chunks

    def align(self) -> TextAlignments:
        if self._alignment.alignments:
            return self._alignment

        response = TextAlignments()
       

       
        query_text_chunks = [x for x in self._input_query_text_chunk_indices]
        target_text_chunks = [x for x in self._input_target_text_chunk_indices]
        

        remaining_query_chunks: List[TextChunk] = [x for x in self._input_query_text_chunk_indices]
        remaining_target_chunks: List[TextChunk] =[x for x in self._input_target_text_chunk_indices]
        matched_text_chunks: List[Tuple[TextChunk, TextChunk]] = []

        # Cycle through the matches and select the first pretty good match
        # This will help favor matching the texts with a preference for similar ordering
        unavailable_query_chunks = set([])
        unavailable_target_chunks = set([])
        match_levels = [75, 65, 55, 45, 35]
        for match_level in match_levels:
            # Stop trying when no further matches are possible
            if len(target_text_chunks) == len(unavailable_target_chunks) or len(
                query_text_chunks
            ) == len(unavailable_query_chunks):
                break

            for target_idx, target_text_chunk in enumerate(target_text_chunks):
                for query_idx, query_text_chunk in enumerate(query_text_chunks):
                    if (
                        target_idx in unavailable_target_chunks
                        or query_idx in unavailable_query_chunks
                    ):
                        continue

                    query_text = "".join(
                        [chr(x) for x in self._query[query_text_chunk.indices]]
                    )
                    target_text = "".join(
                        [chr(x) for x in self._target[target_text_chunk.indices]]
                    )
                    ratio = (
                        fuzz.partial_ratio
                        if len(query_text) < 10 and len(target_text) < 10
                        else fuzz.ratio
                    )
                    pair_distance = ratio(query_text, target_text)

                    if pair_distance > match_level:
                        
                        matched_text_chunks.append(
                            (query_text_chunk, target_text_chunk)
                        )
                        unavailable_query_chunks.add(query_idx)
                        unavailable_target_chunks.add(target_idx)
                        remaining_query_chunks.remove(query_text_chunk)
                        remaining_target_chunks.remove(target_text_chunk)
                        continue

        # Delete Empty lines
        remaining_query_chunks = [x for x in remaining_query_chunks if x.indices]
        remaining_target_chunks = [x for x in remaining_target_chunks if x.indices]

        # If some lines have not been matched, try to find optimal alignment
        if remaining_query_chunks and remaining_target_chunks:
            # Find the optimal matches for the remaining lines
            distances = np.array(
                [
                    np.array(
                        [
                            abs(
                                100
                                - fuzz.ratio(
                                    get_text_chunk_string(self._query, x),
                                    get_text_chunk_string(self._target, y),
                                )
                            )
                            for x in remaining_query_chunks
                        ]
                    )
                    for y in remaining_target_chunks
                ]
            )

            # Accept only those matches with sufficient score
            remaining_indices = linear_sum_assignment(distances)
            delete_remaining_query_chunk_indices: List[int] = []
            delete_remaining_target_chunk_indices: List[int] = []
            for remaining_target_index, remaining_query_index in zip(
                remaining_indices[0], remaining_indices[1]
            ):
                if distances[remaining_target_index][remaining_query_index] < 30:
                    matched_text_chunks.append(
                        (
                            remaining_query_chunks[remaining_query_index],
                            remaining_target_chunks[remaining_target_index],
                        )
                    )

                    pair_distance = (
                        100 - distances[remaining_target_index][remaining_query_index]
                    )
                    
                    delete_remaining_query_chunk_indices.append(remaining_query_index)
                    delete_remaining_target_chunk_indices.append(remaining_target_index)

            for delete_remaining_query_chunk_index in sorted(
                delete_remaining_query_chunk_indices, reverse=True
            ):
                del remaining_query_chunks[delete_remaining_query_chunk_index]

            for delete_remaining_target_chunk_index in sorted(
                delete_remaining_target_chunk_indices, reverse=True
            ):
                del remaining_target_chunks[delete_remaining_target_chunk_index]

            if remaining_query_chunks and remaining_target_chunks:
                
                sorted_remaining_query_chunks = sorted(
                    remaining_query_chunks, key=lambda x: len(x.indices), reverse=True
                )
                sorted_remaining_target_chunks = sorted(
                    remaining_target_chunks, key=lambda x: len(x.indices), reverse=True
                )
                for sorted_remaining_query_chunk, sorted_remaining_target_chunk in zip(
                    sorted_remaining_query_chunks, sorted_remaining_target_chunks
                ):
                    matched_text_chunks.append(
                        (
                            sorted_remaining_query_chunk,
                            sorted_remaining_target_chunk,
                        )
                    )
                    query_string = "".join(
                        [
                            chr(x)
                            for x in self._query[sorted_remaining_query_chunk.indices]
                        ]
                    )
                    target_string = "".join(
                        [
                            chr(x)
                            for x in self._target[sorted_remaining_target_chunk.indices]
                        ]
                    )
                                        
                    remaining_query_chunks.remove(sorted_remaining_query_chunk)
                    remaining_target_chunks.remove(sorted_remaining_target_chunk)
        #For papyroLogos (replace ocr with corresponding transcription line in a potential alignment run)
        self._output_chunks=matched_text_chunks
        self._unaligned_chunks=[x for x in self._input_target_text_chunk_indices if x not in [y[1] for y in self._output_chunks]]  

        # Using the chunked matches get a global alignment between each
        # set of chunks
        for matched_text_chunk in matched_text_chunks:
            self._output_query_text_chunk_indices.append(matched_text_chunk[0])
            self._output_target_text_chunk_indices.append(matched_text_chunk[1])
            _, global_match = global_alignment(
                self._query, self._target, matched_text_chunk
            )
            response.alignments.extend(global_match.alignments)

        self._alignment = response
        self.__aligned_text_chunks = matched_text_chunks
        return self._alignment


def global_alignment(
    query_text: LetterList,
    target_text: LetterList,
    chunk_indices: Tuple[TextChunk, TextChunk],
) -> Tuple[int, TextAlignments]:
    alignments = TextAlignments()
    alignment = needle.NeedlemanWunsch(
        query_text[chunk_indices[0].indices], target_text[chunk_indices[1].indices]
    )
    alignment.align()
    al1, al2 = alignment.get_aligned_sequences("list")
    query_idx = -1
    target_idx = -1
    for query_entity, target_entity in zip(al1, al2):
        query_is_gap = isinstance(query_entity, minineedle.core.Gap)
        target_is_gap = isinstance(target_entity, minineedle.core.Gap)

        if not query_is_gap:
            query_idx += 1
        if not target_is_gap:
            target_idx += 1

        if query_is_gap or target_is_gap:
            continue

        alignments.alignments.append(
            AlignmentKey(
                chunk_indices[0].indices[query_idx],
                chunk_indices[1].indices[target_idx],
            )
        )

    return (alignment.get_score(), alignments)
