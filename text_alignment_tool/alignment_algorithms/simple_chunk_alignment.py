from typing import List, Dict, Tuple
from dataclasses import dataclass
from text_alignment_tool.shared_classes import (
    TextChunk,
    LetterList,
    TextAlignments,
    AlignmentKey,
    )
    
from text_alignment_tool.alignment_algorithms import AlignmentAlgorithm
from text_alignment_tool.analyzers import get_text_chunk_string
from fuzzywuzzy import fuzz
import numpy as np
from scipy.optimize import linear_sum_assignment


@dataclass
class AlignedChunk:
    """A pairwise matching of two chunks within larger texts"""

    query_chunk: TextChunk
    target_chunk: TextChunk


class SimpleChunkAlignmentAlgorithm(AlignmentAlgorithm):
    """Aligns two texts based upon the chunking of the texts.
    It matches chunks using the Hungarian algorithm.
    """

    def __init__(self):
        super().__init__()
        self.__aligned_text_chunks: List[AlignedChunk] = []
        self._output_chunks = []
        self._unaligned_chunks=[]

    @property
    def aligned_text_chunks(self) -> List[AlignedChunk]:
        """A list of the matched pairs of text chunks."""
        if not self.__aligned_text_chunks:
            self.align()

        return self.__aligned_text_chunks

    def align(self) -> TextAlignments:
        if self._alignment.alignments:  # We memoize this method
            return self._alignment

        alignments = TextAlignments()

        # Calculate the matrix of distances between all lines
        distances = np.array(
            [
                np.array(
                    [
                        abs(
                            100
                            - fuzz.partial_ratio(
                                get_text_chunk_string(self._query, x),
                                get_text_chunk_string(self._target, y),
                            )
                        )
                        for x in self._input_query_text_chunk_indices
                    ]
                )
                for y in self._input_target_text_chunk_indices
            ]
        )
        # print(distances) # Visualize the score matrix

        # Calculate the cheapest path through the matrix
        best_matches = linear_sum_assignment(distances)
        # print(best_matches) # See the matches

        # Store the matches since we would otherwise iterate twice over the list
        match_dict: Dict[int, int] = {
            query_index: target_index
            for target_index, query_index in zip(best_matches[0], best_matches[1])
        }
      
       			

        # Add the signwise alignment
        # WARNING: this is a faux-alignment at the letter level,
        # DO NOT use it.
        for query_idx in range(0, len(self._input_query_text_chunk_indices)):
        	#Adding if condition (for papyroLogos) otherwise there's a key error if the target has less lines than query:
            #because some query lines don't get matched to any target lines and stay unmatched.
        	if query_idx in match_dict.keys():
            		alignments.alignments.extend(
                			[
                    		AlignmentKey(q, t)
                   		for q, t in zip(
                        			self._input_query_text_chunk_indices[query_idx].indices,
                        			self._input_target_text_chunk_indices[
                            			match_dict[query_idx]
                        				].indices,
                   				 )
                			]
           		        )

        # Store the chunk-wise alignments, this is the valid output data
        self.__aligned_text_chunks = [
            AlignedChunk(
                query_chunk=self._input_query_text_chunk_indices[query],
                target_chunk=self._input_target_text_chunk_indices[target],
            )
            for query, target in match_dict.items()
        ]
        self._output_chunks=[(x.query_chunk,x.target_chunk) for x in self.__aligned_text_chunks]
        self._unaligned_chunks=[x for x in self._input_target_text_chunk_indices if x not in [y[1] for y in self._output_chunks]]  
       

        # Finish storing the alignments
        self._alignment = alignments
        return self._alignment

